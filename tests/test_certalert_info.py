# -*- coding: utf-8 -*-

"""Tests for the `CertAlertInfo` class."""

from datetime import datetime
from datetime import timezone
from datetime import timedelta

from .context import certalert


def test_certalert_info(cai_i1: certalert.CertAlertInfo) -> None:
    """Test vanilla CertAlertInfo object w/o notifications."""
    assert cai_i1.cert_id == "AA:BB:CC:12:34:DE:AD:BE:EF", \
        ".cert_id must be 'AA:BB:CC:12:34:DE:AD:BE:EF'"
    assert cai_i1.cert_name == "Test Certificate Number 1", \
        ".cert_name must be 'Test Certificate Number 1'"
    assert cai_i1.cert_origin == "pem_dir/intermediate_ca", \
        ".cert_origin must be 'pem_dir/intermediate_ca'"
    assert cai_i1.cert_source == "test_1_cert.pem", \
        ".cert_source must be 'test_1_cert.pem'"
    assert cai_i1.failed is False, "cai_i1 should not be 'failed'"
    assert cai_i1.last_notification is None
    assert cai_i1.last_success_dt is None
    assert cai_i1.last_fail_dt is None
    assert cai_i1.ignored is False
    assert cai_i1.notification_count == 0, \
        "cai_i1 should not have any notifications sent"
    c1_valid_until = datetime(year=2033,
                              month=3,
                              day=6,
                              hour=22,
                              minute=16,
                              second=27,
                              tzinfo=timezone.utc)
    assert cai_i1.valid_until == c1_valid_until, \
        "cert 1 should be valid until 2033-03-06 23:16:27+0100"


def test_certalert_info_w_notifications(cai_i2: certalert.CertAlertInfo) \
        -> None:
    """Test CertAlertInfo object with notifications."""
    assert len(cai_i2.notifications) == 2, \
        "CertAlertInfo 2 should have two notifications"


def test_certalert_info_last_notification(cai_i2: certalert.CertAlertInfo) \
        -> None:
    """Test CertAlertInfo object's `.last_notification`."""
    known_last_n = cai_i2.notifications[1]
    assert len(cai_i2.notifications) == 2, \
        "CertAlertInfo 2 should have two notifications"
    assert cai_i2.last_notification == known_last_n, \
        "CertAlertInfo'2 last notification is not correct (1)"
    # reverse the ordering of the notifications and assert that we
    # still get the same 'last' notification as before
    cai_i2.notifications.reverse()
    assert cai_i2.last_notification == known_last_n, \
        "CertAlertInfo'2 last notification is not correct (2)"


def test_certalert_info_w_last_dts(cai_i3: certalert.CertAlertInfo,
                                   a_date: datetime) -> None:
    """Test CertAlertInfo object created with datetimes.

    last_success_dt and last_fail_dt should be set to `a_date`.
    """
    assert cai_i3.last_success_dt == a_date, \
        f"CertAlertInfo 3's last_success date must be set to {a_date}!"
    assert cai_i3.last_fail_dt == a_date, \
        f"CertAlertInfo 3's last_fail date must be set to {a_date}!"


def test_certalert_info_failed(cai_i4: certalert.CertAlertInfo) -> None:
    """Test CertAlertInfo object's `.failed` property."""
    assert cai_i4.failed, "CertAlertInfo 4 should be 'failed'"
    diff = timedelta(seconds=1)
    cai_i4.last_success_dt = cai_i4.last_fail_dt + diff
    assert cai_i4.failed is False, \
        "CertAlertInfo 4 should NOT be 'failed' after adding a success dt"


def test_certalert_info_add_notify(cai_i1: certalert.CertAlertInfo,
                                   notification_n1: certalert.Notification) \
        -> None:
    """Test adding a notification to a CertAlertInfo object."""
    assert len(cai_i1.notifications) == 0, \
        "vanilla CertAlertInfo should not have any notifications"
    cai_i1.add_notification(notification_n1)
    assert len(cai_i1.notifications) == 1, \
        "vanilla CertAlertInfo should have a notification after adding one"


def test_serialisation(cai_i1: certalert.CertAlertInfo,
                       cai_i1_data: dict):
    """Ensure marshmallow can serialize `CertAlertInfo`."""
    # now = datetime.now(timezone.utc)
    cai_schema = certalert.CertAlertInfoSchema()
    json_data = cai_schema.dump(cai_i1)
    cai_i1_data["last_fail_dt"] = None
    cai_i1_data["last_success_dt"] = None
    cai_i1_data["notifications"] = []
    cai_i1_data["last_notification"] = {}
    cai_i1_data["notification_count"] = 0
    cai_i1_data["ignored"] = False
    # rename `valid_from` and `valid_until` to `not_before` and `not_after`
    # `valid_from` is None, so no `.isoformat()` is needed or possible
    cai_i1_data["not_before"] = cai_i1_data.pop("valid_from")
    # `valid_until` is a datetime object and should be converted to a
    # string; using `.isoformat()` for this
    cai_i1_data["not_after"] = cai_i1_data.pop("valid_until").isoformat()
    assert json_data == cai_i1_data, "serialisation not successful"


def test_serialisation_w_notification(cai_i5: certalert.CertAlertInfo,
                                      cai_i5_data: dict,
                                      b_date: datetime):
    """Ensure marshmallow can serialize `CertAlertInfo` w/ notif."""
    ignore_fields = ["notification_count", "notifications"]
    cai_schema = certalert.CertAlertInfoSchema(exclude=ignore_fields)
    json_data = cai_schema.dump(cai_i5)
    cai_i5_data["last_success_dt"] = None
    cai_i5_data["not_before"] = cai_i5_data.pop("valid_from").isoformat()
    cai_i5_data["not_after"] = cai_i5_data.pop("valid_until").isoformat()
    cai_i5_data["last_fail_dt"] = None
    cai_i5_data["ignored"] = False
    cai_i5_data.pop("notifications")
    cai_i5_data["last_notification"] = {
        "created_at": b_date.isoformat(),
        "failed": None,
        "notify_type": "NOTIFY",
        "sent": None}
    assert json_data == cai_i5_data, \
        "serialisation w/ notification not successful"


def test_deserialisation(cai_i2: certalert.CertAlertInfo,
                         cai_i2_data: dict):
    """Ensure marshmallow can deserialize `CertAlertInfo` JSON."""
    cai_schema = certalert.CertAlertInfoSchema()
    # cai_i2_data is a dict with 'last_success_dt' as a datetime,
    # when deserialising, 'last_success_dt' comes from JSON and is a
    # string, so we are converting this here
    cai_i2_data["last_success_dt"] = str(cai_i2_data.get("last_success_dt"))
    # ..._data["notifications"] is a list of `Notification` objects, but
    # when deserialising it should be a list of dicts; so we convert
    # the objects to dicts here using `dump_notifications()`
    cai_i2_data["notifications"] = cai_schema.dump_notifications(cai_i2)
    # `.valid_from` and `valid_until` are datetime objects and must be
    # converted to strings; also they must be renamed to `not_before`
    # and `not_after` (cai_i2_data is meant to be used for
    # CertAlertInfo(**data), not for ...Schema.load(data))
    cai_i2_data["not_before"] = cai_i2_data.pop("valid_from").isoformat()
    cai_i2_data["not_after"] = cai_i2_data.pop("valid_until").isoformat()
    cai2 = cai_schema.load(cai_i2_data)
    assert cai2.cert_id == cai_i2.cert_id, \
        "deserialisation not successful (cert_id)"
    assert cai2.cert_name == cai_i2.cert_name, \
        "deserialisation not successful (cert_name)"
    assert cai2.cert_origin == cai_i2.cert_origin, \
        "deserialisation not successful (cert_origin)"
    assert cai2.cert_source == cai_i2.cert_source, \
        "deserialisation not successful (cert_source)"
