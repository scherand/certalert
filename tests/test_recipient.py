# -*- coding: utf-8 -*-

"""Tests for the `Recipient` class."""

from .context import certalert


def test_recipient(recipient_r1: certalert.Recipient,
                   recipient_r2: certalert.Recipient,
                   recipient_r3: certalert.Recipient) -> None:
    """Test recipient object."""
    r = recipient_r1
    assert r.name == "Tester 1", ".name should be 'Tester 1'"
    assert r.email == "tester-1@example.com", (".email should be "
                                               "'tester-1@example.com'")
    assert r.is_default, "should be default recipient"
    assert r.threema_id is None, ".threema_id should be None"
    assert str(r) == "!Tester 1 <tester-1@example.com>", \
        ("__str__() should return '!Tester 1 <tester-1@example.com>'")
    assert repr(r).endswith(str(r)), \
        f"return value of __repr__() should end in {str(r)}"

    r = recipient_r2
    assert r.name == "Tester 2", ".name should be 'Tester 2'"
    assert r.email == "tester-2@test.example.com", \
        (".email should be 'tester-2@test.example.com'")
    assert r.is_default is False, "should NOT be default recipient"
    assert r.threema_id is None, ".threema_id should be None"

    r = recipient_r3
    assert r.name == "Tester 3", ".name should be 'Tester 3'"
    assert r.email == "tester-3@test.example.com", \
        (".email should be 'tester-3@test.example.com'")
    assert r.is_default is False, "should NOT be default recipient"
    assert r.threema_id == "Prima3", ".threema_id should be 'Prima3'"
    assert repr(r).endswith(str(r)), \
        f"return value of __repr__() should end in {str(r)}"


def test_serialisation(recipient_r1: certalert.Recipient,
                       recipient_r1_data: dict):
    """Ensure marshmallow can serialize `Recipient`."""
    r_schema = certalert.RecipientSchema()
    json_data = r_schema.dump(recipient_r1)
    recipient_r1_data["threema_id"] = None
    assert json_data == recipient_r1_data, "serialisation not successful"


def test_deserialisation(recipient_r1: certalert.Recipient,
                         recipient_r1_data: dict):
    """Ensure marshmallow can deserialize `Recipient` JSON."""
    r_schema = certalert.RecipientSchema()
    r1 = r_schema.load(recipient_r1_data)
    assert r1.name == recipient_r1.name, \
        "deserialisation not successful (name)"
    assert r1.email == recipient_r1.email, \
        "deserialisation not successful (email)"
    assert r1.threema_id == recipient_r1.threema_id, \
        "deserialisation not successful (threema_id)"
    assert r1.is_default == recipient_r1.is_default, \
        "deserialisation not successful (is_default)"
