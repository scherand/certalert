# -*- coding: utf-8 -*-

"""Tests for the `Notification` class."""

from datetime import datetime
from datetime import timezone

from .context import certalert


def test_notification(notification_n1: certalert.Notification) -> None:
    """Test notification object."""
    n = notification_n1
    assert n.cert_id == "AB:CD:EF:12:34:DE:AD:BE:EF", \
        ".cert_id should be 'AB:CD:EF:12:34:DE:AD:BE:EF'"
    assert n.cert_name == "Test Certificate Number 1", \
        ".cert_name should be 'Test Certificate Number 1'"
    assert n.notify_type == "ALERT", ".notify_type should be 'ALERT'"
    assert n.priority == 1000, ".priority should be 1000"
    assert n.recipients == [], "list of recipients should be empty"
    assert n.sent is None, "sent dt should be None"
    assert n.is_sent is False, "notification is not sent"
    assert n.failed is None, "failed dt should be None"
    assert n.send_failed is False, "sending notification did not fail"
    assert n.subject is None, "subject should be None"
    assert n.message is None, "message should be None"
    assert str(n) == f"{n.notify_type} for {n.cert_name}", \
        f"__str__() should return '{n.notify_type} for {n.cert_name}'"
    assert repr(n) == str(n), \
        "__repr__() should return the same string as __str__()"


def test_notification_w_recs(notification_n2: certalert.Notification) -> None:
    """Test notification object when instantiated with recipients."""
    n = notification_n2
    assert len(n.recipients) == 3, \
        "notification 2 should have three recipients"


def test_notification_w_created(notification_n3: certalert.Notification,
                                a_date: datetime) -> None:
    """Test notification object when instantiated with created_at."""
    n = notification_n3
    assert n.created_at == a_date, \
        "notification 3 should have created_at in the past"


def test_add_recipient(notification_n1: certalert.Notification,
                       recipient_r1: certalert.Recipient) -> None:
    """Test adding a recipient to a notification works."""
    n = notification_n1
    assert len(n.recipients) == 0, \
        "vanialla notification should not have any recipients"
    n.add_recipient(recipient_r1)
    assert len(n.recipients) == 1, \
        "notification should have one recipient after adding one"


def test_serialisation(notification_n1: certalert.Notification,
                       notification_n1_data: dict):
    """Ensure marshmallow can serialize `Recipient`."""
    now = datetime.now(timezone.utc)
    n_schema = certalert.NotificationSchema()
    json_data = n_schema.dump(notification_n1)
    notification_n1_data["priority"] = 1000
    notification_n1_data["sent"] = None
    notification_n1_data["failed"] = None
    notification_n1_data["recipients"] = []
    created = notification_n1.created_at
    ca_ts_s = notification_n1.created_at.isoformat()
    notification_n1_data["created_at"] = ca_ts_s
    assert json_data == notification_n1_data, "serialisation not successful"
    t_diff_s = (now - created).total_seconds()
    assert t_diff_s < 1, "created_at should be 'near' now"


def test_deserialisation(notification_n2: certalert.Recipient,
                         notification_n2_data: dict):
    """Ensure marshmallow can deserialize `Notification` JSON."""
    n_schema = certalert.NotificationSchema()
    # ..._data["recipients"] is a list of `Recipient` objects, but
    # when deserialising it should be a list of dicts; so we convert
    # the objects to dicts here using `dump_recipients()`
    notification_n2_data["recipients"] = n_schema.dump_recipients(
        notification_n2)
    n2 = n_schema.load(notification_n2_data)
    assert n2.cert_id == notification_n2.cert_id, \
        "deserialisation not successful (cert_id)"
    assert n2.cert_name == notification_n2.cert_name, \
        "deserialisation not successful (name)"
    assert n2.notify_type == notification_n2.notify_type, \
        "deserialisation not successful (notify_type)"
    assert len(n2.recipients) == 3, \
        "deserialisation not successful (3 recipients)"
    assert isinstance(n2.recipients[0], certalert.Recipient)
