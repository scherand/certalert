# -*- coding: utf-8 -*-

"""Provide common data for all tests.

See
https://docs.pytest.org/en/7.2.x/how-to/fixtures.html#scope-sharing-fixtures-across-classes-modules-packages-or-session
"""
import pytest
import datetime

from collections.abc import Iterable
from pathlib import Path

from .context import certalert


def mock_mask_function(*args, **kwargs) -> None:
    """Disable the call to a function.

    Will be used to prevent the side effect (creating a file on
    disk) of CertAlertStore's `._persist()`.
    """
    pass


# monkeypatch `_persist()` so our `mock_mask_function` is called
# instead of the "real" `_persist()`.
@pytest.fixture
def disarm_store_persist(monkeypatch):
    monkeypatch.setattr(certalert.CertAlertStore,
                        "_persist",
                        mock_mask_function)


@pytest.fixture
def a_date() -> datetime.datetime:
    dt = datetime.datetime(year=2021,
                           month=7,
                           day=26,
                           hour=2,
                           minute=6,
                           second=15,
                           tzinfo=datetime.timezone.utc)
    return dt


@pytest.fixture
def b_date() -> datetime.datetime:
    dt = datetime.datetime(year=2022,
                           month=8,
                           day=27,
                           hour=3,
                           minute=7,
                           second=16,
                           tzinfo=datetime.timezone.utc)
    return dt


@pytest.fixture
def cert_t1() -> dict:
    """Data for test certificate 1.

    No valid_from(_str/_dt).
    """
    data = {
        "serial": "testcert_1_serial",
        "issuer": "test-infrastructure",
        "fingerprint": "AB:CD:EF:12:34:DE:AD:BE:EF",
        "name": "Test Certificate Number 1",
        "origin": "config",
        "source": "conftest-cert_t1",
        "valid_until_str": "2033-03-06 23:16:27+0100"
    }
    return data


@pytest.fixture
def cert_t2() -> dict:
    """Data for test certificate 2."""
    data = {
        "serial": "testcert_2_serial",
        "issuer": "test-infrastructure",
        "fingerprint": "EF:BE:AD:ED:34:12:EF:CD:AB",
        "name": "Test Certificate Number 2",
        "origin": "config",
        "source": "conftest-cert_t2",
        "valid_from_str": "2018-05-28 13:26:00+0200",
        "valid_until_str": "2028-05-28 13:26:47+0200"
    }
    return data


@pytest.fixture
def cert_t3() -> dict:
    """Data for test certificate 3.

    Contradicting valid_from/_to _str/_dt.
    """
    data = {
        "serial": "testcert_3_serial",
        "issuer": "test-infrastructure",
        "fingerprint": "AA:BB:CC:12:34:DE:AD:BE:EF",
        "name": "Test Certificate Number 3",
        "origin": "config",
        "source": "conftest-cert_t3",
        "valid_from_str": "2018-05-28 13:26:00+0200",
        "valid_from_dt": datetime.datetime(year=2017,
                                           month=5,
                                           day=28,
                                           hour=11,
                                           minute=26,
                                           second=0,
                                           tzinfo=datetime.timezone.utc),
        "valid_until_str": "2028-05-28 13:26:47+0200",
        "valid_until_dt": datetime.datetime(year=2027,
                                            month=5,
                                            day=28,
                                            hour=11,
                                            minute=26,
                                            second=47,
                                            tzinfo=datetime.timezone.utc),
    }
    return data


@pytest.fixture
def cert_t4() -> dict:
    """Data for test certificate 4.

    valid_from/valid_until _str w/o timezone.
    """
    data = {
        "serial": "testcert_4_serial",
        "issuer": "test-infrastructure",
        "fingerprint": "BA:CB:ED:12:34:DE:AD:BE:EF",
        "name": "Test Certificate Number 4",
        "origin": "config",
        "source": "conftest-cert_t4",
        "valid_from_str": "2000-1-31 00:00:01",
        "valid_until_str": "2002-1-30 23:59:59",
    }
    return data


@pytest.fixture
def cert_missing_valid_until() -> dict:
    """Data similar to test certificate 1, missing valid_until."""
    data = {
        "serial": "testcert_1_serial",
        "issuer": "test-infrastructure",
        "fingerprint": "AB:CD:EF:12:34:DE:AD:BE:EF",
        "name": "Test Certificate Number 1",
        "origin": "config",
        "source": "conftest-cert_t1",
    }
    return data


@pytest.fixture
def cert_incorrect_valid_until_str() -> dict:
    """Data similar to test certificate 1, incorrect valid_until_str."""
    data = {
        "serial": "testcert_1_serial",
        "issuer": "test-infrastructure",
        "fingerprint": "AB:CD:EF:12:34:DE:AD:BE:EF",
        "name": "Test Certificate Number 1",
        "origin": "config",
        "source": "conftest-cert_t1",
        "valid_until_str": "this-is-not-a-date"
    }
    return data


@pytest.fixture
def recipient_r1_data() -> dict:
    """Data for recipient 1."""
    data = {
        "name": "Tester 1",
        "email": "tester-1@example.com",
        "is_default": True,
    }
    return data


@pytest.fixture
def recipient_r1(recipient_r1_data: dict) -> certalert.Recipient:
    """Object for recipient 1."""
    return certalert.Recipient(**recipient_r1_data)


@pytest.fixture
def recipient_r2_data() -> dict:
    """Data for recipient 2."""
    data = {
        "name": "Tester 2",
        "email": "tester-2@test.example.com",
        "is_default": False,
    }
    return data


@pytest.fixture
def recipient_r2(recipient_r2_data: dict) -> certalert.Recipient:
    """Object for recipient 2."""
    return certalert.Recipient(**recipient_r2_data)


@pytest.fixture
def recipient_r3_data() -> dict:
    """Data for recipient 3."""
    data = {
        "name": "Tester 3",
        "email": "tester-3@test.example.com",
        "threema_id": "Prima3",
    }
    return data


@pytest.fixture
def recipient_r3(recipient_r3_data: dict) -> certalert.Recipient:
    """Object for recipient 3."""
    return certalert.Recipient(**recipient_r3_data)


@pytest.fixture
def recipients(
        recipient_r1: certalert.Recipient,
        recipient_r2: certalert.Recipient,
        recipient_r3: certalert.Recipient) -> Iterable[certalert.Recipient]:
    """List of recipients."""
    rs = [recipient_r1, recipient_r2, recipient_r3]
    return rs


@pytest.fixture
def notification_n1_data() -> dict:
    """Data for notification 1."""
    data = {
        "cert_id": "AB:CD:EF:12:34:DE:AD:BE:EF",
        "cert_name": "Test Certificate Number 1",
        "notify_type": "ALERT",
    }
    return data


@pytest.fixture
def notification_n1(notification_n1_data: dict) -> certalert.Notification:
    """Object for notification 1."""
    return certalert.Notification(**notification_n1_data)


@pytest.fixture
def notification_n2_data() -> dict:
    """Data for notification 2."""
    data = {
        "cert_id": "EF:BE:AD:ED:34:12:EF:CD:AB",
        "cert_name": "Test Certificate Number 2",
        "notify_type": "HEADS_UP",
    }
    return data


@pytest.fixture
def notification_n2(notification_n2_data: dict,
                    recipients: Iterable[certalert.Recipient]) \
        -> certalert.Notification:
    """Object for notification 2."""
    data = notification_n2_data
    data["recipients"] = recipients
    return certalert.Notification(**data)


@pytest.fixture
def notification_n3_data() -> dict:
    """Data for notification 3."""
    data = {
        "cert_id": "AA:BB:CC:12:34:DE:AD:BE:EF",
        "cert_name": "Test Certificate Number 3",
        "notify_type": "NOTIFY",
    }
    return data


@pytest.fixture
def notification_n3(notification_n3_data: dict,
                    a_date: datetime.datetime) -> certalert.Notification:
    """Object for notification 3."""
    data = notification_n3_data
    data["created_at"] = a_date
    return certalert.Notification(**data)


@pytest.fixture
def notification_n4_data(b_date: datetime.datetime) -> dict:
    """Data for notification 4."""
    data = {
        "cert_id": "BA:CB:ED:12:34:DE:AD:BE:EF",
        "cert_name": "Test Certificate Number 4",
        "notify_type": "NOTIFY",
        "created_at": b_date,
    }
    return data


@pytest.fixture
def notification_n4(notification_n4_data: dict,
                    a_date: datetime.datetime) -> certalert.Notification:
    """Object for notification 4."""
    data = notification_n4_data
    return certalert.Notification(**data)


@pytest.fixture
def cai_i1_data() -> dict:
    """Data for CertAlertInfo 1."""
    data = {
        "cert_id": "AA:BB:CC:12:34:DE:AD:BE:EF",
        "cert_name": "Test Certificate Number 1",
        "cert_origin": "pem_dir/intermediate_ca",
        "cert_source": "test_1_cert.pem",
        "valid_from": None,
        "valid_until": datetime.datetime(year=2033,
                                         month=3,
                                         day=6,
                                         hour=22,
                                         minute=16,
                                         second=27,
                                         tzinfo=datetime.timezone.utc),
    }
    return data


@pytest.fixture
def cai_i1(cai_i1_data: dict) -> certalert.Notification:
    """Object for CertAlertInfo 1."""
    data = cai_i1_data
    return certalert.CertAlertInfo(**data)


@pytest.fixture
def cai_i2_data(notification_n1: certalert.Notification,
                notification_n2: certalert.Notification,
                a_date: datetime.datetime) -> dict:
    """Data for CertAlertInfo 2 (w/ notifications)."""
    data = {
        "cert_id": "EF:BE:AD:ED:34:12:EF:CD:AB",
        "cert_name": "Test Certificate Number 2",
        "cert_origin": "pem_dir/intermediate_ca",
        "cert_source": "test_2_cert.pem",
        "valid_from": datetime.datetime(year=2018,
                                        month=5,
                                        day=28,
                                        hour=11,
                                        minute=26,
                                        second=0,
                                        tzinfo=datetime.timezone.utc),
        "valid_until": datetime.datetime(year=2018,
                                         month=5,
                                         day=28,
                                         hour=11,
                                         minute=26,
                                         second=47,
                                         tzinfo=datetime.timezone.utc),
        "notifications": [notification_n1, notification_n2],
        "last_success_dt": a_date,
    }
    return data


@pytest.fixture
def cai_i2(cai_i2_data: dict) -> certalert.Notification:
    """Object for CertAlertInfo 2."""
    data = cai_i2_data
    return certalert.CertAlertInfo(**data)


@pytest.fixture
def cai_i3_data(a_date: datetime.datetime) -> dict:
    """Data for CertAlertInfo 3 (w/ last_success_dt)."""
    data = {
        "cert_id": "AA:BB:CC:12:34:DE:AD:BE:EF",
        "cert_name": "Test Certificate Number 3",
        "cert_origin": "https://example.com",
        "cert_source": "example.com",
        "valid_from": datetime.datetime(year=2017,
                                        month=5,
                                        day=28,
                                        hour=11,
                                        minute=26,
                                        second=00,
                                        tzinfo=datetime.timezone.utc),
        "valid_until": datetime.datetime(year=2027,
                                         month=5,
                                         day=28,
                                         hour=11,
                                         minute=26,
                                         second=47,
                                         tzinfo=datetime.timezone.utc),
        "last_success_dt": a_date,
        "last_fail_dt": a_date,
    }
    return data


@pytest.fixture
def cai_i3(cai_i3_data: dict) -> certalert.Notification:
    """Object for CertAlertInfo 3."""
    data = cai_i3_data
    return certalert.CertAlertInfo(**data)


@pytest.fixture
def cai_i4_data(a_date: datetime.datetime) -> dict:
    """Data for CertAlertInfo 4 (w/ last_fail_dt)."""
    data = {
        "cert_id": "BA:CB:ED:12:34:DE:AD:BE:EF",
        "cert_name": "Test Certificate Number 4",
        "cert_origin": "https://test.example.com",
        "cert_source": "test.example.com",
        "valid_from": datetime.datetime(year=2000,
                                        month=1,
                                        day=31,
                                        hour=0,
                                        minute=0,
                                        second=1,
                                        tzinfo=datetime.timezone.utc),
        "valid_until": datetime.datetime(year=2002,
                                         month=1,
                                         day=30,
                                         hour=23,
                                         minute=59,
                                         second=59,
                                         tzinfo=datetime.timezone.utc),
        "last_fail_dt": a_date,
    }
    return data


@pytest.fixture
def cai_i4(cai_i4_data: dict) -> certalert.Notification:
    """Object for CertAlertInfo 4."""
    data = cai_i4_data
    return certalert.CertAlertInfo(**data)


@pytest.fixture
def cai_i5_data(notification_n3: certalert.Notification,
                notification_n4: certalert.Notification) -> dict:
    """Data for CertAlertInfo 5."""
    data = {
        "cert_id": "BA:CB:ED:12::34:12:EF:CD:AB",
        "cert_name": "Test Certificate Number 5",
        "cert_origin": "network",
        "cert_source": "foobarbaz.example.com",
        "valid_from": datetime.datetime(year=2000,
                                        month=1,
                                        day=31,
                                        hour=0,
                                        minute=0,
                                        second=1,
                                        tzinfo=datetime.timezone.utc),
        "valid_until": datetime.datetime(year=2002,
                                         month=1,
                                         day=30,
                                         hour=23,
                                         minute=59,
                                         second=59,
                                         tzinfo=datetime.timezone.utc),
        "notifications": [notification_n4, notification_n3],
    }
    return data


@pytest.fixture
def cai_i5(cai_i5_data: dict) -> certalert.Notification:
    """Object for CertAlertInfo 5."""
    data = cai_i5_data
    return certalert.CertAlertInfo(**data)


@pytest.fixture
def pem_dir_path() -> Path:
    """Pathlib object to tests PEM dir's path."""
    return Path("tests/data/pem")


@pytest.fixture
def index_dir_path() -> Path:
    """Pathlib object to tests index.txt dir's path."""
    return Path("tests/data/idx")


@pytest.fixture
def revoked_url_w_schema() -> str:
    """'Full' URL to badssl's revoked cert."""
    return "https://revoked.badssl.com"


@pytest.fixture
def revoked_url_wo_schema() -> str:
    """Domain only URL to badssl's revoked cert."""
    return "revoked.badssl.com"


@pytest.fixture
def invalid_hostname_url() -> str:
    """URL to an inexistent host."""
    return "nothingthere.gahtsno.example.com"


@pytest.fixture
def conn_refused_url() -> str:
    """URL that times out."""
    return "127.0.0.1"


@pytest.fixture
def certalert_store_config() -> dict:
    """Provide a test config for CertAlertStore."""
    test_certs_valid_from = datetime.datetime(year=2001,
                                              month=6,
                                              day=22,
                                              hour=11,
                                              minute=13,
                                              second=17,
                                              tzinfo=datetime.timezone.utc)
    today = datetime.datetime.now(datetime.timezone.utc)
    days_100 = datetime.timedelta(days=100)
    days_85 = datetime.timedelta(days=85)
    days_24 = datetime.timedelta(days=24)
    days_12 = datetime.timedelta(days=12)
    days_6 = datetime.timedelta(days=6)
    config = {
        "certalert_db_path": "certalert_test.json",
        # recipients is called notification_recipients in YAML config
        "recipients": [
            {
                "name": "admin",
                "email": "certalert-admin@mailinator.com",
                "default": True,
            },
        ],
        "notification_schemes": [
            {
                "name": "default",
                "heads_up_days": 90,
                "notification_threshold": 30,
                "notification_frequency": 5,
                "warning_threshold": 14,
                "warning_frequency": 2,
                "alert_threshold": 7,
                "alert_frequency": 1,
                "post_mortem_duration": 30,
            },
        ],
        "dirs_notification_templates_email": [
            {
                "dir": "tests/data/email_templates",
            },
        ],
        "dirs_pem": [
            {
                "dir": "tests/data/pem",
            },
        ],
        "dirs_index": [
            {
                "dir": "tests/data/idx",
            },
        ],
        "urls": [
            {
                "domain": "httpbin.org",
                "notification_scheme": "default",
            },
            {
                "domain": "127.0.0.1",
            },
        ],
        "smtp_server": {
            "host": "localhost.local",
            "port": 25,
            "username": "john.deere",
            "password": "makeItFlat",
            "sender": "Cert Alert <certalert@example.com>",
        },
        "certificates": [
            {
                "fingerprint": None,
                "serial": "00:B0",
                "issuer": "ca.example.com",
                "name": "CN:www.example.com",
                "origin": "conftest.py",
                "source": "CN:www.example.com",
                "valid_from_str": "2023-02-27 09:10:11+03:00",
                "valid_until_str": "2123-03-27T08:09:10Z",
                "recipients": ["admin", "bdmin"],
            },
            {
                "fingerprint":
                    ("AF:8D:E0:C3:D8:D3:E5:58:A7:C0:22:3C:2E:73:ED:9C:"
                     "15:B8:66:31"),
                "serial": "00:B1",
                "issuer": "ca2.example.com",
                "name": "CN:www.example2.com",
                "origin": "conftest.py",
                "source": "CN:www.example2.com",
                "valid_from_str": "2010-02-20 10:11:12+01:00",
                "valid_until_str": "2021-02-19T00:01:00Z",
            },
            {
                "fingerprint": "12:35:71:10",
                "serial": "00:C0",
                "issuer": "ca.example.com",
                "name": "test-still-valid-for-long",
                "origin": "conftest.py",
                "source": "dict3",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today + days_100,
            },
            {
                "fingerprint": "12:35:71:11",
                "serial": "00:C1",
                "issuer": "ca.example.com",
                "name": "test-valid-for-heads-up",
                "origin": "conftest.py",
                "source": "dict4",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today + days_85,
            },
            {
                "fingerprint": "12:35:71:12",
                "serial": "00:C2",
                "issuer": "ca.example.com",
                "name": "test-valid-for-notify",
                "origin": "conftest.py",
                "source": "dict5",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today + days_24,
            },
            {
                "fingerprint": "12:35:71:13",
                "serial": "00:C3",
                "issuer": "ca.example.com",
                "name": "test-valid-for-warning",
                "origin": "conftest.py",
                "source": "dict6",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today + days_12,
            },
            {
                "fingerprint": "12:35:71:14",
                "serial": "00:C4",
                "issuer": "ca.example.com",
                "name": "test-valid-for-alert",
                "origin": "conftest.py",
                "source": "dict7",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today + days_6,
            },
            {
                "fingerprint": "12:35:71:15",
                "serial": "00:C5",
                "issuer": "ca.example.com",
                "name": "test-post-mortem",
                "origin": "conftest.py",
                "source": "dict8",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today - days_6,
            },
            {
                "fingerprint": "12:35:71:16",
                "serial": "00:C6",
                "issuer": "ca.example.com",
                "name": "test-past-post-mortem",
                "origin": "conftest.py",
                "source": "dict9",
                "valid_from_dt": test_certs_valid_from,
                "valid_until_dt": today - days_85,
            },
        ],
    }
    return config
