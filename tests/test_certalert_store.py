# -*- coding: utf-8 -*-

"""Tests for the `CertAlertStore` class."""

import logging
import pytest
import tempfile
# from datetime import datetime
# from datetime import timezone
# from datetime import timedelta

from .context import certalert


def test_certalert_store(certalert_store_config: dict,
                         disarm_store_persist) -> None:
    """Test CertAlertStore object."""

    store = certalert.CertAlertStore(config=certalert_store_config)
    assert store
    # store loads:
    # * nine certs from config (defined in `conftest.py`)
    # * one certificate from PEM (tests/data/pem)
    # * one certificate from index (tests/data/idx)
    # * one certificate from network (httpbin.org)
    store.load(load_from_config=True)
    assert len(store.certificates) == 12, "should load twelve certificates"
    ns = store.prepare_notifications()
    assert len(ns) == 5, "should prepare five notifications"
    assert len(store._notifications) == 5, \
        "store should have five notifications"
    store.clear_notifications()
    assert len(store._notifications) == 0, "store should clear notifications"
    c = store._get_certificate(cert_id="invalid_id")
    assert c is None, "invalid_id cert should not be found (private)"
    ais = store.find_ai_by_cert_id_prefix(prefix="invalid_id")
    assert len(ais) == 0, "invalid_id cert should not be found (public)"
    valid_cert_id = ("AF:8D:E0:C3:D8:D3:E5:58:A7:C0:22:3C:2E:73:ED:9C:"
                     "15:B8:66:31")
    c = store._get_certificate(cert_id=valid_cert_id)
    assert isinstance(c, certalert.Cert), "shoud find a Cert object (private)"
    ais = store.find_ai_by_cert_id_prefix(prefix=valid_cert_id[:2])
    assert len(ais) == 0, "too short prefix should not find cert (public)"
    ais = store.find_ai_by_cert_id_prefix(prefix=valid_cert_id[:8])
    assert len(ais) == 1, "should find one cert with prefix search"
    assert isinstance(ais[0], certalert.CertAlertInfo), \
        "shoud find a CertAlert object (public)"
    ais = store.find_ai_by_cert_id_prefix(prefix="12:35:71")
    assert len(ais) == 7, "should find seven cert with prefix '12:35:71'"
    assert c.name == "CN:www.example2.com", \
        "cert_name should be CN:www.example2.com"
    ai = store._get_alert_info(cert_id=valid_cert_id)
    assert ai, "should find AlertInfo for valid cert (from config)"
    assert len(store.ignored_cert_ids) == 0, \
        "store should not have any ignored cert_ids"
    eml = store._notification_to_eml(sender="certalert@mailinator.com",
                                     recipient="foo@mailinator.com",
                                     notification=ns[0])
    assert eml.startswith("From: certalert@mailinator.com"), \
        "email notification should start with From..."

    # test the notifications and alert info
    # valid-for-long cert
    ai = store._get_alert_info(cert_id="12:35:71:10")
    assert len(ai.notifications) == 0, \
        "valid-for-long cert should not have any notifications"
    # heads-up cert
    ai = store._get_alert_info(cert_id="12:35:71:11")
    assert len(ai.notifications) == 1, \
        "valid-for-heads-up cert should have a notification"
    n = ai.notifications[0]
    assert n.notify_type == "HEADS_UP", \
        "valid-for-heads-up cert should have a HEADS_UP notification"
    # notify cert
    ai = store._get_alert_info(cert_id="12:35:71:12")
    assert len(ai.notifications) == 1, \
        "notify cert should have a notification"
    n = ai.notifications[0]
    assert n.notify_type == "NOTIFY", \
        "valid-for-notify cert should have a NOTIFY notification"
    # warning cert
    ai = store._get_alert_info(cert_id="12:35:71:13")
    assert len(ai.notifications) == 1, \
        "warning cert should have a notification"
    n = ai.notifications[0]
    assert n.notify_type == "WARNING", \
        "valid-for-warning cert should have a WARNING notification"
    # alert cert
    ai = store._get_alert_info(cert_id="12:35:71:14")
    assert len(ai.notifications) == 1, \
        "alert cert should have a notification"
    n = ai.notifications[0]
    assert n.notify_type == "ALERT", \
        "valid-for-alert cert should have a ALERT notification"
    # post-mortem cert
    ai = store._get_alert_info(cert_id="12:35:71:15")
    assert len(ai.notifications) == 1, \
        "post-mortem cert should have a notification"
    n = ai.notifications[0]
    assert n.notify_type == "POST_MORTEM", \
        "post-mortem cert should have a POST_MORTEM notification"
    # past-post-mortem cert
    ai = store._get_alert_info(cert_id="12:35:71:16")
    assert len(ai.notifications) == 0, \
        "past-post-mortem cert should not have a notification"
    assert len(store.ignored_cert_ids) == 0, \
        "store should not have any ignored cert"
    store.ignore_cert(cert_id="foo12:35")
    assert len(store.ignored_cert_ids) == 0, \
        "store should not ignore with unknown cert id"
    store.ignore_cert(cert_id="12:35")
    assert len(store.ignored_cert_ids) == 0, \
        "store should not ignore with ambigous cert id"
    store.ignore_cert(cert_id="12:35:71:15")
    assert len(store.ignored_cert_ids) == 1, \
        "store should have one ignored cert"
    assert store.ignored_cert_ids[0] == "12:35:71:15", \
        "store's ignored cert should have Id '12:35:71:15'"
    store.ignore_cert(cert_id="12:35:71:15")
    assert len(store.ignored_cert_ids) == 1, \
        "store should still have one ignored cert"
    assert store.ignored_cert_ids[0] == "12:35:71:15", \
        "store's still ignored cert should have Id '12:35:71:15'"
    store.unignore_cert(cert_id="foo12:35")
    assert len(store.ignored_cert_ids) == 1, \
        "store should not unignore unknown cert id"
    store.unignore_cert(cert_id="12:35")
    assert len(store.ignored_cert_ids) == 1, \
        "store should still have one ignored cert"
    store.unignore_cert(cert_id="12:35:71:15")
    assert len(store.ignored_cert_ids) == 0, \
        "store should not have any ignored cert again"
    store.unignore_cert(cert_id="12:35:71:15")
    assert len(store.ignored_cert_ids) == 0, \
        "store should still not have any ignored cert again"


def test_get_db_migration_steps(certalert_store_config: dict) -> None:
    """Test CertAlertStore's `_get_migration_steps` method."""
    store = certalert.CertAlertStore(config=certalert_store_config)
    dbv = ["1.0.0", "1.1.0", "1.2.0", "1.2.1", "1.2.3", "2.0.1"]
    certalert.CertAlertStore.__ALL_DB_VERSIONS__ = dbv
    m_steps = store._get_db_migration_steps("1.0.0", "1.1.0")
    assert m_steps == [("1.0.0", "1.1.0")], \
        "migration path should be '1.0.0' > '1.1.0'"
    m_steps = store._get_db_migration_steps("1.0.0", "1.2.0")
    assert m_steps == [("1.0.0", "1.1.0"), ("1.1.0", "1.2.0")], \
        "migration path should be ['1.0.0' > '1.1.0', '1.1.0' > '1.2.0']"
    m_steps = store._get_db_migration_steps("1.0.0", "1.2.1")
    assert m_steps == [("1.0.0", "1.1.0"),
                       ("1.1.0", "1.2.0"),
                       ("1.2.0", "1.2.1")], \
        ("migration path should be ['1.0.0' > '1.1.0', '1.1.0' > '1.2.0', "
         "'1.2.0' > '1.2.1']")
    m_steps = store._get_db_migration_steps("1.2.0", "2.0.1")
    assert m_steps == [("1.2.0", "1.2.1"),
                       ("1.2.1", "1.2.3"),
                       ("1.2.3", "2.0.1")], \
        ("migration path should be ['1.2.0' > '1.2.1', '1.2.1' > '1.2.3', "
         "'1.2.3' > '2.0.1']")
    with pytest.raises(ValueError):
        store._get_db_migration_steps("1.0.0", "5.0.1")
    with pytest.raises(ValueError):
        store._get_db_migration_steps("1.1.1", "2.0.1")
    with pytest.raises(ValueError):
        store._get_db_migration_steps("2.3.4", "4.4.4")
    with pytest.raises(ValueError):
        store._get_db_migration_steps("1.2.3", "1.2.1")


def test_certalert_store_with_ignore(certalert_store_config: dict,
                                     disarm_store_persist) -> None:
    """Test CertAlertStore object when it has an ignored cert."""

    store = certalert.CertAlertStore(config=certalert_store_config)
    store.load(load_from_config=True)
    assert len(store.certificates) == 12, "should load twelve certificates"
    ai = store.find_ai_by_cert_id_prefix(prefix="12:35:71:14")
    assert len(ai) == 1, \
        "there should be exactly one certificate info for Id '12:35:71:14'"
    store.ignore_cert(cert_id="12:35:71:14")
    ns = store.prepare_notifications()
    assert len(ns) == 4, "should prepare four notifications (1 ignored cert)"
    assert len(store._notifications) == 4, \
        "store should have four notifications"


def test_dummy_notification(certalert_store_config: dict) -> None:
    """Test CertAlertStore object's dummy notification."""
    store = certalert.CertAlertStore(config=certalert_store_config)
    assert len(store._notifications) == 0, "should not have any notifications"
    store.prepare_dummy_notification(recipient_email="foo@mailinator.com")
    assert len(store._notifications) == 1, "should not have one notification"
    n = store._notifications[0]
    assert n.recipients[0].email == "foo@mailinator.com", \
        "dummy notification should be for 'foo@mailinator.com'"


def test_certalert_store_purge(certalert_store_config: dict,
                               disarm_store_persist) -> None:
    """Test CertAlertStore's `.purge_certificate` method."""

    store = certalert.CertAlertStore(config=certalert_store_config)
    store.load(load_from_config=True)
    assert len(store.certificates) == 12, "should load twelve certificates"
    assert len(store.alert_infos) == 12, "should load twelve alert info"
    ai = store.find_ai_by_cert_id_prefix(prefix="12:35:71:13")
    assert len(ai) == 1, \
        ("there should still be exactly one certificate info for Id "
         "'12:35:71:13'")
    store.purge_certificate(cert_id="foo12:35:71:13")
    assert len(store.alert_infos) == 12, "should still have 12 alert info"
    store.purge_certificate(cert_id="12:35:71:13")
    assert len(store.alert_infos) == 11, "should now have 11 alert info"
    ai = store.find_ai_by_cert_id_prefix(prefix="12:35:71:13")
    assert len(ai) == 0, \
        ("certificate info for Id '12:35:71:13' should be gone")


def test_certalert_store_multi_purge(certalert_store_config: dict,
                                     disarm_store_persist) -> None:
    """Test CertAlertStore's `.purge_certificate` method."""

    store = certalert.CertAlertStore(config=certalert_store_config)
    store.load(load_from_config=True)
    assert len(store.certificates) == 12, "should load twelve certificates"
    assert len(store.alert_infos) == 12, "should load twelve alert info"
    ais = store.find_ai_by_cert_id_prefix(prefix="12:35:71")
    assert len(ais) == 7, \
        ("there should still be exactly seven certificate info for Id prefix "
         "'12:35:71'")
    c_ids = [ai.cert_id for ai in ais]
    store.purge_certificates(cert_ids=c_ids)
    assert len(store.alert_infos) == 5, "should now have five alert info"
    ai = store.find_ai_by_cert_id_prefix(prefix="12:35:71:12")
    assert len(ai) == 0, \
        ("certificate info for Id '12:35:71:13' should be gone")


def test_certalert_store_missing_notification_scheme(
        certalert_store_config: dict) -> None:
    """Test CertAlertStore's `.prepare_notifications()` method.

    But when a notification scheme cannot be found.
    """

    store = certalert.CertAlertStore(config=certalert_store_config)
    store.load(load_from_config=True)
    c = store._get_certificate(cert_id="12:35:71:11")
    c.notification_scheme_name = "ns_does_not_exist"
    with pytest.raises(RuntimeError):
        store.prepare_notifications()


def test_certalert_store_existing_database(
        certalert_store_config: dict) -> None:
    """Test CertAlertStore with an existing database.

    The `._persist()` call is redirected to the tmp dir.
    """

    certalert_store_config["certalert_db_path"] = \
        "tests/data/certalert_test_3318b439.json"
    store = certalert.CertAlertStore(config=certalert_store_config)
    store.load()
    # fiddle with the 'certalert_db_path' to redirect the persist call
    db_tmp = tempfile.NamedTemporaryFile(delete=False)
    store.config["certalert_db_path"] = db_tmp.name

    assert len(store.alert_infos) == 12, "store must have twelve alert info"
    ns = store.prepare_notifications()
    assert len(ns) == 0, "should not prepare any notifications"
    assert len(store._notifications) == 0, \
        "store should have no notifications"
    certs_w_prefix = store.find_ai_by_cert_id_prefix(prefix="12:35:71:15")
    assert len(certs_w_prefix) == 1, \
        "cert Id '12:35:71:15' should be unique"
    cert_w_notification = certs_w_prefix[0]
    assert cert_w_notification, \
        "certificate with Id '12:35:71:15' should exist and be found"
    assert cert_w_notification.notification_count == 1, \
        "cert '12:35:71:15' should have one notification"

    # the value of 'certalert_db_path' was changed earlier, so the test
    # database is not overwritten
    store._persist()
    store._persist(backup=True)


def test_readonly_mode(certalert_store_config: dict,
                       caplog) -> None:
    """Test call to _persist() in read-only mode."""

    store = certalert.CertAlertStore(config=certalert_store_config)
    store.read_only = True
    store._persist()
    # assert "operating in read-only mode, not persisting data" in caplog.text
    expected_log_tup = ("certalert.certalert_store",
                        logging.WARNING,
                        "operating in read-only mode, not persisting data")
    assert expected_log_tup in caplog.record_tuples


def test_certalert_store_inexistent_database(
        certalert_store_config: dict,
        caplog) -> None:
    """Test CertAlertStore with an incorrect path to the database."""

    certalert_store_config["certalert_db_path"] = \
        "tests/data/certalert_inexistent.json"
    certalert.CertAlertStore(config=certalert_store_config)
    expected_log_tup = ("certalert.certalert_store",
                        logging.WARNING,
                        ("cannot find database "
                         "'tests/data/certalert_inexistent.json'"))
    assert expected_log_tup in caplog.record_tuples


def test_certalert_store_load_email_templates(
        certalert_store_config: dict,
        caplog) -> None:
    """Test CertAlertStore can load email templates."""

    certalert_store_config["certalert_db_path"] = \
        "tests/data/certalert_test_d5d49f39.json"
    store = certalert.CertAlertStore(config=certalert_store_config)
    assert len(store.email_templates) == 2, \
        "should load one email template (twice, once as 'default')"
