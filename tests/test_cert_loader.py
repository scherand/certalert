# -*- coding: utf-8 -*-

"""Tests for the classes of the `cert_loader` module."""

import logging
import pytest

from pathlib import Path
from datetime import datetime
from datetime import timezone

from .context import certalert


def test_cert_loader(recipient_r1: certalert.Recipient) -> None:
    """Test instantiating `CertLoader`.

    `CertLoader is the parent class of all loaders and should directly
    be instantiated.
    """
    loader = certalert.CertLoader(recipients=[recipient_r1])
    with pytest.raises(NotImplementedError):
        loader.load()


def test_pem_loader_invalid_dir(pem_dir_path: Path,
                                recipient_r1: certalert.Recipient) -> None:
    """Test PEMLoader object w/ invalid dir path."""
    pem_dir_path = pem_dir_path / "inexistent"
    loader = certalert.PEMLoader(recipients=[recipient_r1],
                                 dir_path=pem_dir_path)
    certs = loader.load()
    assert len(certs) == 0, \
        "should not find certificates in incorrect test PEM dir"


def test_pem_loader(pem_dir_path: Path,
                    recipient_r1: certalert.Recipient) -> None:
    """Test PEMLoader object."""
    loader = certalert.PEMLoader(recipients=[recipient_r1],
                                 dir_path=pem_dir_path)
    certs = loader.load()
    assert len(certs) == 1, "should find one certificate in test PEM dir"
    assert isinstance(certs[0], certalert.Cert)


def test_url_loader(revoked_url_w_schema: str,
                    revoked_url_wo_schema: str,
                    invalid_hostname_url: str,
                    conn_refused_url: str,
                    recipient_r1: certalert.Recipient,
                    caplog,
                    monkeypatch) -> None:
    """Test URLLoader object."""

    # -------------
    # START MOCKING
    # -------------
    # it would be possible to move the mock(ing) to `conftest.py`:
    # https://docs.pytest.org/en/7.1.x/how-to/monkeypatch.html#monkeypatching-returned-objects-building-mock-classes

    # will receive the monkeypatched object (`URLLoader`) as *arg and
    # the remaining parameters of the "original" function call in
    # **kwargs
    def mockreturn(*args, **kwargs) -> str:
        """Mock result of network call.

        Depending on the value of the **kwargs (domain, port, timeout),
        construct a "known" return value.

        Note that for the test runs, the value for `timeout` is
        functionally ignored (i.e. the "timeout error" is returned
        immediately). The timeout value is, however, used in log
        messages.
        """
        default_pem = b"""-----BEGIN CERTIFICATE-----
MIIFSzCCBDOgAwIBAgIQSueVSfqavj8QDxekeOFpCTANBgkqhkiG9w0BAQsFADCB
kDELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G
A1UEBxMHU2FsZm9yZDEaMBgGA1UEChMRQ09NT0RPIENBIExpbWl0ZWQxNjA0BgNV
BAMTLUNPTU9ETyBSU0EgRG9tYWluIFZhbGlkYXRpb24gU2VjdXJlIFNlcnZlciBD
QTAeFw0xNTA0MDkwMDAwMDBaFw0xNTA0MTIyMzU5NTlaMFkxITAfBgNVBAsTGERv
bWFpbiBDb250cm9sIFZhbGlkYXRlZDEdMBsGA1UECxMUUG9zaXRpdmVTU0wgV2ls
ZGNhcmQxFTATBgNVBAMUDCouYmFkc3NsLmNvbTCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAMIE7PiM7gTCs9hQ1XBYzJMY61yoaEmwIrX5lZ6xKyx2PmzA
S2BMTOqytMAPgLaw+XLJhgL5XEFdEyt/ccRLvOmULlA3pmccYYz2QULFRtMWhyef
dOsKnRFSJiFzbIRMeVXk0WvoBj1IFVKtsyjbqv9u/2CVSndrOfEk0TG23U3AxPxT
uW1CrbV8/q71FdIzSOciccfCFHpsKOo3St/qbLVytH5aohbcabFXRNsKEqveww9H
dFxBIuGa+RuT5q0iBikusbpJHAwnnqP7i/dAcgCskgjZjFeEU4EFy+b+a1SYQCeF
xxC7c3DvaRhBB0VVfPlkPz0sw6l865MaTIbRyoUCAwEAAaOCAdUwggHRMB8GA1Ud
IwQYMBaAFJCvajqUWgvYkOoSVnPfQ7Q6KNrnMB0GA1UdDgQWBBSd7sF7gQs6R2lx
GH0RN5O8pRs/+zAOBgNVHQ8BAf8EBAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUE
FjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwTwYDVR0gBEgwRjA6BgsrBgEEAbIxAQIC
BzArMCkGCCsGAQUFBwIBFh1odHRwczovL3NlY3VyZS5jb21vZG8uY29tL0NQUzAI
BgZngQwBAgEwVAYDVR0fBE0wSzBJoEegRYZDaHR0cDovL2NybC5jb21vZG9jYS5j
b20vQ09NT0RPUlNBRG9tYWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNybDCB
hQYIKwYBBQUHAQEEeTB3ME8GCCsGAQUFBzAChkNodHRwOi8vY3J0LmNvbW9kb2Nh
LmNvbS9DT01PRE9SU0FEb21haW5WYWxpZGF0aW9uU2VjdXJlU2VydmVyQ0EuY3J0
MCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5jb21vZG9jYS5jb20wIwYDVR0RBBww
GoIMKi5iYWRzc2wuY29tggpiYWRzc2wuY29tMA0GCSqGSIb3DQEBCwUAA4IBAQBq
evHa/wMHcnjFZqFPRkMOXxQhjHUa6zbgH6QQFezaMyV8O7UKxwE4PSf9WNnM6i1p
OXy+l+8L1gtY54x/v7NMHfO3kICmNnwUW+wHLQI+G1tjWxWrAPofOxkt3+IjEBEH
fnJ/4r+3ABuYLyw/zoWaJ4wQIghBK4o+gk783SHGVnRwpDTysUCeK1iiWQ8dSO/r
ET7BSp68ZVVtxqPv1dSWzfGuJ/ekVxQ8lEEFeouhN0fX9X3c+s5vMaKwjOrMEpsi
8TRwz311SotoKQwe6Zaoz7ASH1wq7mcvf71z81oBIgxw+s1F73hczg36TuHvzmWf
RwxPuzZEaFZcVlmtqoq8
-----END CERTIFICATE-----
        """
        pem = default_pem
        domain = kwargs.get("domain")
        port = kwargs.get("port")
        timeout = kwargs.get("timeout")
        if (domain == "revoked.badssl.com") and (port == 443):
            # we want to return the default PEM data
            pass
        elif (domain == "revoked.badssl.com") and (port == 25):
            # simulating a connection timeout
            msg = (f"failed to connect to '{domain}' (timeout: {timeout} "
                   "second(s))")
            logging.warning(msg)
            raise certalert.CertLoadError(msg)
        elif domain == "127.0.0.1":
            msg = (f"failed to fetch certificate for '{domain}', "
                   "connection refused")
            logging.warning(msg)
            raise certalert.CertLoadError(msg)
        elif domain == "nothingthere.gahtsno.example.com":
            msg = (f"failed to fetch certificate for '{domain}', "
                   "invalid domain?")
            logging.warning(msg)
            raise certalert.CertLoadError(msg)
        return pem

    # monkeypatch `_cert_from_net_as_pem` so our `mockreturn` is called
    # instead of the "real" `_cert_from_net_as_pem`.
    monkeypatch.setattr(certalert.URLLoader,
                        "_cert_from_net_as_pem",
                        mockreturn)
    # -----------
    # END MOCKING
    # -----------

    loader = certalert.URLLoader(recipients=[recipient_r1])
    # test loading a certificate from a URL w/ a "https://" prefix
    cert_1 = loader.load(url=revoked_url_w_schema)
    assert cert_1, "should find one certificate in URL test (1)"
    # test loading a certificate from a URL w/o the "https://" prefix
    cert_2 = loader.load(url=revoked_url_wo_schema)
    assert cert_2, "should find one certificate in URL test (2)"
    assert cert_1.cert_id == cert_2.cert_id, \
        "cert_1 and cert_2 should be the same"
    # test loading a certificate from an invalid host name
    cert_3 = loader.load(url=invalid_hostname_url)
    assert cert_3 is None
    log_rec = caplog.records[-1]
    assert log_rec.msg == ("failed to fetch certificate for "
                           "'nothingthere.gahtsno.example.com', "
                           "invalid domain?"), \
        "log text for invalid hostname is not correct"
    # attempting to connect to a port that is allowed by the firewall
    # but not running anything that accepts SSL connections results in
    # a timeout
    cert_4 = loader.load(url=revoked_url_wo_schema,
                         port=25,
                         timeout=1)
    assert cert_4 is None
    log_rec = caplog.records[-1]
    assert log_rec.msg == ("failed to connect to 'revoked.badssl.com' "
                           "(timeout: 1 second(s))"), \
        "log text for connection timeout is not correct"
    # attempting to connect to a port that is allowed by the firewall
    # but not running anything that accepts SSL connections results in
    # a timeout
    cert_5 = loader.load(url=conn_refused_url,
                         port=42,
                         timeout=1)
    assert cert_5 is None
    log_rec = caplog.records[-1]
    assert log_rec.msg == ("failed to fetch certificate for "
                           "'127.0.0.1', connection refused"), \
        "log text for connection refused is not correct"


def test_index_loader(index_dir_path: Path,
                      recipient_r1: certalert.Recipient) -> None:
    """Test IndexLoader object."""
    loader = certalert.IndexLoader(recipients=[recipient_r1],
                                   dir_path=index_dir_path)
    certs = loader.load()
    assert len(certs) == 1, "should find one certificate in test Index dir"
    cert = certs[0]
    assert isinstance(cert, certalert.Cert)
    assert cert.cert_id == "issuer_of_tests/data/idx/index.txt::1015", \
        "cert_id should be issuer_of_tests/data/idx/index.txt::1015"
    ex_dt = datetime(year=2027,
                     month=8,
                     day=17,
                     hour=15,
                     minute=23,
                     second=59,
                     tzinfo=timezone.utc)
    assert cert.valid_until_dt == ex_dt, \
        "cert should be valid until 2027-08-17T15:23:59Z"


def test_index_loader_invalid_dir(index_dir_path: Path,
                                  recipient_r1: certalert.Recipient) -> None:
    """Test IndexLoader object w/ invalid dir path."""
    index_dir_path = index_dir_path / "inexistent"
    loader = certalert.IndexLoader(recipients=[recipient_r1],
                                   dir_path=index_dir_path)
    certs = loader.load()
    assert len(certs) == 0, \
        "should not find certificates in incorrect test Index dir"
