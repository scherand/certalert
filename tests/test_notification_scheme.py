# -*- coding: utf-8 -*-

"""Tests for the `NotificationScheme` class."""

from datetime import datetime
from datetime import timezone
from datetime import timedelta

from .context import certalert


def test_notification_scheme() -> None:
    """Test NotificationScheme object."""
    ns = certalert.NotificationScheme(name="test_scheme")
    assert ns.name == "test_scheme", "name should be 'test_scheme'"
    assert ns.heads_up_days == 90, "heads_up_days should be 90"
    assert ns.notification_threshold == 30, \
        "notification_threshold should be 90"
    assert ns.notification_frequency == 5, \
        "notification_frequency should be 5"
    assert ns.warning_threshold == 14, \
        "warning_threshold should be 14"
    assert ns.warning_frequency == 2, \
        "warning_frequency should be 2"
    assert ns.alert_threshold == 7, \
        "alert_threshold should be 7"
    assert ns.alert_frequency == 1, \
        "alert_frequency should be 1"
    assert ns.post_mortem_duration == 30, "post_mortem_duration should be 30"
    assert str(ns) == "test_scheme 30/14/7", \
        "__str__() should be 'test_scheme 30/14/7'"
    assert repr(ns).endswith(str(ns)), "__repr__() should end with str()"


def test_notification_type(a_date: datetime) -> None:
    """Test NotificationScheme object's `get_notification_type`."""
    ns = certalert.NotificationScheme(name="test_scheme")
    t = ns.get_notification_type(target_dt=a_date)
    assert t == "POST_MORTEM", "t should be POST_MORTEM"
    now = datetime.now(timezone.utc)
    tomorrow = now + timedelta(days=1)
    t = ns.get_notification_type(target_dt=tomorrow,
                                 reference_dt=now)
    assert t == "ALERT", "t should be ALERT"
    t_plus_10 = now + timedelta(days=10)
    t = ns.get_notification_type(target_dt=t_plus_10,
                                 reference_dt=now)
    assert t == "WARNING", "t should be WARNING"
    t_plus_28 = now + timedelta(days=28)
    t = ns.get_notification_type(target_dt=t_plus_28,
                                 reference_dt=now)
    assert t == "NOTIFY", "t should be NOTIFY"
    t_plus_33 = now + timedelta(days=33)
    t = ns.get_notification_type(target_dt=t_plus_33,
                                 reference_dt=now)
    assert t == "HEADS_UP", "t should be HEADS_UP"
    t_plus_150 = now + timedelta(days=150)
    t = ns.get_notification_type(target_dt=t_plus_150,
                                 reference_dt=now)
    assert t is None, "t should be None"
