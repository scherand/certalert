# -*- coding: utf-8 -*-

"""Provide `context` for other tests.

See https://docs.python-guide.org/writing/structure/#test-suite
"""
import pytest
import datetime

from collections.abc import Iterable

from .context import certalert


def test_missing_valid_until(cert_missing_valid_until: dict) -> None:
    """Without valid_until, `Cert` must not be instantiated."""
    with pytest.raises(RuntimeError):
        certalert.Cert(**cert_missing_valid_until)


def test_cert_fp_and_id(cert_t1: dict) -> None:
    """Test `.fp` and `.cert_id` properties of `Cert`."""
    c = certalert.Cert(**cert_t1)
    assert c
    assert c.fp == cert_t1.get("fingerprint"), (".fp must be equal to "
                                                ".fingerprint")
    assert c.cert_id == c.fp, ".cert_id must be equal to fingerprint"
    c.fingerprint = None  # hacky, (sh/c)ould not happen in "real life"
    issuer = cert_t1.get("issuer")
    serial = cert_t1.get("serial")
    assert c.cert_id == f"{issuer}::{serial}", (".cert_id must be equal to "
                                                "issuer::serial")


def test_cert_id_missing_issuer(cert_t1: dict) -> None:
    """Test `.id` property of `Cert` w/o fp and issuer."""
    c = certalert.Cert(**cert_t1)
    c.fingerprint = None
    c.issuer = None
    with pytest.raises(RuntimeError):
        c.cert_id


def test_cert_id_missing_serial(cert_t1: dict) -> None:
    """Test `.id` property of `Cert` w/o fp and serial."""
    c = certalert.Cert(**cert_t1)
    c.fingerprint = None
    c.serial = None
    with pytest.raises(RuntimeError):
        c.cert_id


def test_human_name(cert_t1: dict) -> None:
    """Test `.human_name` property of `Cert`."""
    c = certalert.Cert(**cert_t1)
    name = cert_t1.get("name")
    serial = cert_t1.get("serial")
    fp_short = cert_t1.get("fingerprint")[:14]
    assert c.human_name == f"{name} [{serial}, {fp_short}]", \
        ("`.human_name` must be "
         "'Test Certificate Number 1 [testcert_1_serial, AB:CD:EF:12:34]'")
    c.fingerprint = None
    c.human_name == "unknown fingerprint"


def test_add_recipient(cert_t1: dict,
                       recipient_r1: certalert.Recipient) -> None:
    """Test `.add_recipient()` function."""
    c = certalert.Cert(**cert_t1)
    assert len(c.recipients) == 0, ("newly created Cert must not have any "
                                    "recipient")
    c.add_recipient(recipient_r1)
    assert len(c.recipients) == 1, ("Cert must now have one (1) recipient")


def test_add_recipients(cert_t1: dict,
                        recipients: Iterable[certalert.Recipient]) -> None:
    """Test `.add_recipients()` function."""
    c = certalert.Cert(**cert_t1)
    assert len(c.recipients) == 0, ("newly created Cert must not have any "
                                    "recipient")
    r3 = recipients.pop()
    c.add_recipients(recipients)
    assert len(c.recipients) == 2, ("Cert must now have two (2) recipients")
    c.add_recipients([r3])
    assert len(c.recipients) == 3, ("Cert must now have three (3) recipients")


def test_add_recipient_and_recipients(cert_t1: dict,
                                      recipients: Iterable[certalert.Recipient]) -> None:  # noqa E501
    """Test mix of `.add_recipient()` and `.add_recipients()` functions."""
    c = certalert.Cert(**cert_t1)
    assert len(c.recipients) == 0, ("newly created Cert must not have any "
                                    "recipient")
    r1 = recipients.pop(0)
    c.add_recipient(r1)
    assert len(c.recipients) == 1, ("Cert must now have one (1) recipient")
    c.add_recipients(recipients)
    assert len(c.recipients) == 3, ("Cert must now have three (3) recipients")


def test_days_left(cert_t1: dict) -> None:
    """Test `.days_left` property of `Cert`."""
    c = certalert.Cert(**cert_t1)
    dl = c.days_left
    ref_dt = datetime.datetime.now(datetime.timezone.utc)
    ref_dl = (c.valid_until_dt - ref_dt).days
    assert dl == ref_dl, (f"`.days_left` should be {ref_dl}")


def test_str_equals_human_name(cert_t1: dict) -> None:
    """Test "stringification" of `Cert`."""
    c = certalert.Cert(**cert_t1)
    assert str(c) == c.human_name


def test_valid_from_parsing(cert_t2: dict) -> None:
    """Test if string input valid_from is correctly parsed."""
    c = certalert.Cert(**cert_t2)
    vf = datetime.datetime(year=2018,
                           month=5,
                           day=28,
                           hour=11,
                           minute=26,
                           second=0,
                           tzinfo=datetime.timezone.utc)
    assert c.valid_from_dt == vf, f"`valid_from_dt` must be {vf}"


def test_valid_from_dt_and_string(cert_t3: dict) -> None:
    """Test if string input valid_until is correctly ignored."""
    c = certalert.Cert(**cert_t3)
    vf = datetime.datetime(year=2017,
                           month=5,
                           day=28,
                           hour=11,
                           minute=26,
                           second=0,
                           tzinfo=datetime.timezone.utc)
    assert c.valid_from_dt == vf, f"`valid_from_dt` must be {vf}"


def test_valid_until_parsing(cert_t1: dict) -> None:
    """Test if string input valid_until is correctly parsed."""
    c = certalert.Cert(**cert_t1)
    vu = datetime.datetime(year=2033,
                           month=3,
                           day=6,
                           hour=22,
                           minute=16,
                           second=27,
                           tzinfo=datetime.timezone.utc)
    assert c.valid_until_dt == vu, f"`valid_until_dt` must be {vu}"


def test_valid_until_dt_and_string(cert_t3: dict) -> None:
    """Test if string input valid_until is correctly ignored."""
    c = certalert.Cert(**cert_t3)
    vu = datetime.datetime(year=2027,
                           month=5,
                           day=28,
                           hour=11,
                           minute=26,
                           second=47,
                           tzinfo=datetime.timezone.utc)
    assert c.valid_until_dt == vu, f"`valid_until_dt` must be {vu}"


def test_invalid_valid_until_str(cert_incorrect_valid_until_str: dict) -> None:
    """Test if incorrect string for valid_until raises exception."""
    with pytest.raises(RuntimeError):
        certalert.Cert(**cert_incorrect_valid_until_str)


def test_valid_from_str_no_tz(cert_t4: dict) -> None:
    """Test if valid_from_str w/o timezone is handled correctly."""
    c = certalert.Cert(**cert_t4)
    vf = datetime.datetime(year=2000,
                           month=1,
                           day=31,
                           hour=0,
                           minute=0,
                           second=1,
                           tzinfo=datetime.timezone.utc)
    assert c.valid_from_dt == vf, f"`valid_from_dt` must be {vf}"


def test_valid_until_str_no_tz(cert_t4: dict) -> None:
    """Test if valid_until_str w/o timezone is handled correctly."""
    c = certalert.Cert(**cert_t4)
    vu = datetime.datetime(year=2002,
                           month=1,
                           day=30,
                           hour=23,
                           minute=59,
                           second=59,
                           tzinfo=datetime.timezone.utc)
    assert c.valid_until_dt == vu, f"`valid_until_dt` must be {vu}"


def test_repr(cert_t2: dict) -> None:
    """Test if __repr__() function is returning same as __str__()."""
    c = certalert.Cert(**cert_t2)
    assert repr(c) == str(c), ("__repr__() and __str__() must return "
                               "the same string")
