# -*- coding: utf-8 -*-

"""Script to control `certalert`."""

import sys
import logging
import logging.config
import argparse
import pprint

from pathlib import Path
from ruamel.yaml import YAML

from .certalert_store import CertAlertStore
from .certalert_info import CertAlertInfoSchema


def main():
    """Start certalert."""
    # how to use "add_subparsers" to handle subcommands:
    # https://docs.python.org/3.10/library/argparse.html#sub-commands
    parser = argparse.ArgumentParser()
    sub_desc = ("All calls to certalert (except a plain "
                "certalert.certalert -h/-v) require one of the following "
                "subcommands")
    subparsers = parser.add_subparsers(description=sub_desc)

    # create the parser for the "notify" (sub)command
    p_notify = subparsers.add_parser("notify",
                                     help="start a notification run")
    p_notify.set_defaults(func=notify)

    # create the parser for the "show" (sub)command
    p_show = subparsers.add_parser("show",
                                   help=("show information about known "
                                         "certificates (cached information "
                                         "only)"))
    p_show.add_argument("--json",
                        action="store_true",
                        help="produce JSON output")
    p_show.set_defaults(func=show)

    # create the parser for the "migrate" (sub)command
    p_migrate = subparsers.add_parser("migrate",
                                      help=("migrate the database version; "
                                            "current database will be backed "
                                            "up"))
    p_migrate.set_defaults(func=migrate)

    # create the parser for the "ignore" (sub)command
    p_ignore = subparsers.add_parser("ignore",
                                     help=("manage 'ignore status' of the "
                                           "certificate with id 'Id'"))
    p_ignore.set_defaults(func=ignore)
    p_ignore_args = p_ignore.add_mutually_exclusive_group(required=True)
    p_ignore_args.add_argument("-a",
                               "--add",
                               type=str,
                               help=("certificate Id to add to the list of "
                                     "ingored certificate Ids; you can "
                                     "provide only a prefix of the Id as "
                                     "long as the prefix it is not ambigous"))

    p_ignore_args.add_argument("-r",
                               "--remove",
                               type=str,
                               help=("certificate Id to remove from to the "
                                     "list of ingored certificate Ids; you "
                                     "can provide only a prefix of the Id as "
                                     "long as the prefix it is not ambigous"))

    p_ignore_args.add_argument("-l",
                               "--list",
                               action="store_true",
                               help=("show the list of currently ignored "
                                     "certificate Ids"))
    p_ignore_args.add_argument("--json",
                               action="store_true",
                               help=("produce JSON output (only used with "
                                     "--list)"))

    # create the parser for the "purge" (sub)command
    p_purge = subparsers.add_parser("purge",
                                    help=("purge all state/information for "
                                          "certificate with the given Id "
                                          "from the database; note that a "
                                          "new history will be created "
                                          "during the next `notify` run if "
                                          "the source of the certificate "
                                          "prevails"))
    p_purge.set_defaults(func=purge)
    p_purge.add_argument("id",
                         type=str,
                         help=("certificate Id to purge all information for; "
                               "you can provide only a prefix of the Id as "
                               "long as the prefix it is not ambigous"))

    p_purge.add_argument("--force",
                         action="store_true",
                         help=("do not ask for confirmation before purging "
                               "if prefix matches multiple certificate Ids, "
                               "just purge all information"))

    # create the parser for the "test-email" (sub)command
    p_email = subparsers.add_parser("test-email",
                                    help=("send a test email notification"))
    p_email.set_defaults(func=test_email)
    p_email.add_argument("recipient",
                         type=str,
                         help=("the recipient's email address"))

    p_email.add_argument("--notification-type",
                         type=str,
                         default="NOTIFY",
                         help=("type of the notification that is sent; "
                               "default 'NOTIFY'"))

    parser.add_argument("-v",
                        "--version",
                        action="store_true",
                        help="output `certalert`'s' version number and exit")
    parser.add_argument("--verbose",
                        action="store_true",
                        help="be verbose (debug)")
    parser.add_argument("-c",
                        "--config",
                        type=str,
                        default="certalert.yml",
                        help=("name and possibly relative path to the YAML "
                              "file with the `certalert` config; default "
                              "'certalert.yml'"))
    parser.add_argument("--include-certs-from-config",
                        action="store_true",
                        help=("if set, certificates defined in 'certificates' "
                              "in the config YAML will (also) be loaded"))

    args = parser.parse_args()

    if args.verbose:
        log_conf["loggers"]["__main__"]["level"] = logging.DEBUG
        loggers = [
            "certalert.cert_loader",
            "certalert.certalert_cert",
            "certalert.certalert_info",
            "certalert.certalert_store",
            "certalert.notification",
            "certalert.notification_scheme",
            "certalert.recipient",
        ]
        for lgr in loggers:
            log_conf["loggers"].get(lgr)["level"] = logging.DEBUG
    logging.config.dictConfig(log_conf)
    log = logging.getLogger(__name__)
    log.info(f"certalert v{__VERSION__}, starting up")
    log.debug("running in verbose mode")

    if args.version:
        msg = f"certalert v{__VERSION__}"
        log.info(msg)
        print(msg)
    else:
        # call the function that was specified for the subcommand we are
        # executing
        try:
            args.func(args)
        except AttributeError as e:
            log.debug(e)
            msg = ("error calling `certalert` command (did you specify a "
                   "subcommand?); see -h/--help for more information and/or "
                   "use verbose mode (--verbose) to see more information "
                   "about the error")
            log.warning(msg)
            print(msg)

    log.info("shutting down")


def migrate(args: argparse.Namespace) -> None:
    """Entry point for `certalert`'s "migrate' subcommand."""

    log = logging.getLogger(__name__)
    config = _load_config(fp_config_file=args.config)
    store = CertAlertStore(config=config)

    store.read_only = True
    log.info("migrating database (if required)")

    store.load(load_from_config=args.include_certs_from_config)
    store.read_only = False
    store._migrate_db_if_required(persist=True, backup=True)


def notify(args: argparse.Namespace) -> None:
    """Entry point for `certalert`'s "notify" subcommand."""

    log = logging.getLogger(__name__)
    config = _load_config(fp_config_file=args.config)
    store = CertAlertStore(config=config)

    log.debug(f"we know the following recipients: {store.recipients}")
    log.debug("we know the following notification schemes: "
              f"{store.notification_schemes}")

    log.info("starting notification run")
    store.load(load_from_config=args.include_certs_from_config)
    store.prepare_notifications()
    store.send_notifications()


def show(args: argparse.Namespace) -> None:
    """Entry point for `certalert`'s "show" subcommand."""

    log = logging.getLogger(__name__)
    config = _load_config(fp_config_file=args.config)
    store = CertAlertStore(config=config)

    use_json = args.json
    if use_json:
        log.debug("producing JSON output (--json)")

    store.read_only = True  # safety measure
    log.info("displaying/outputting cached information about certificates "
             "(show)")
    if use_json:
        schema = CertAlertInfoSchema(only=AI_JSON_FIELDS, many=True)
        print(schema.dumps(store.alert_infos))
    else:
        print("=====================================")
        print("          KNOWN CERTIFICATES         ")
        print("=====================================")
        for ai in store.alert_infos:
            print(ai)
        print("=====================================")


def ignore(args: argparse.Namespace) -> None:
    """Entry point for `certalert`'s "ignore" subcommand."""

    log = logging.getLogger(__name__)
    config = _load_config(fp_config_file=args.config)
    store = CertAlertStore(config=config)

    if args.list:
        log.info("listing currently ignored certificate Ids")
        use_json = args.json
        if use_json:
            log.debug("producing JSON output (--json)")
        store.read_only = True  # safety measure
        if use_json:
            schema = CertAlertInfoSchema(only=AI_JSON_FIELDS, many=True)
            ais = [ai for ai in store.alert_infos if ai.ignored]
            print(schema.dumps(ais))
        else:
            at_least_one = False
            print("==========================================")
            print("          INGORED CERTIFICATE IDS         ")
            print("==========================================")
            for ai in store.alert_infos:
                if not ai.ignored:
                    continue
                if ai.last_notification:
                    last_type = ai.last_notification.notify_type
                else:
                    last_type = "n/a"
                if ai.ignored:
                    disbl_indicator = "*"
                else:
                    disbl_indicator = " "
                msg = (f"{disbl_indicator}{ai.cert_name}, "
                       f"{len(ai.notifications)} notification(s), last "
                       f"processed notification ({last_type}): "
                       f"{ai.last_success_dt}")
                print(msg)
                at_least_one = True
            if not at_least_one:
                print("no certificated Ids are currently ignored")
            print("==========================================")

    elif args.add:
        prefix = args.add
        ca_infos = store.find_ai_by_cert_id_prefix(prefix=prefix)
        if len(ca_infos) > 1:
            log.warning(f"multiple certificates match prefix {prefix} "
                        "(your argument value for --ignore); "
                        "can only use prefix if it is not ambigous")
        elif len(ca_infos) == 1:
            ignored = store.ignored_cert_ids
            c_id = ca_infos[0].cert_id
            if c_id in ignored:
                log.info(f"certificate Id {c_id} already ignored")
            else:
                store.ignore_cert(cert_id=c_id)
        elif len(ca_infos) == 0:
            if len(prefix) < 3:
                log.warning(f"prefix {prefix} for cert_id is too short, must "
                            "be at least three characters")
            else:
                log.info(f"no certificate matches prefix {prefix} "
                         "(the argument value for --ignore)")

    elif args.remove:
        prefix = args.remove
        ca_infos = store.find_ai_by_cert_id_prefix(prefix=prefix)
        if len(ca_infos) > 1:
            log.warning(f"multiple certificates match prefix {prefix} "
                        "(the argument value for --no-ignore); "
                        "can only use prefix if it is not ambigous")
        elif len(ca_infos) == 1:
            ignored = store.ignored_cert_ids
            c_id = ca_infos[0].cert_id
            if c_id in ignored:
                store.unignore_cert(cert_id=c_id)
            else:
                log.info(f"certificate Id {c_id} is already not ignored")
        elif len(ca_infos) == 0:
            if len(prefix) < 3:
                log.warning(f"prefix {prefix} for cert_id is too short, must "
                            "be at least three characters")
            else:
                log.info(f"no certificate matches prefix {prefix} "
                         "(the argument value for --no-ignore)")


def purge(args: argparse.Namespace) -> None:
    """Entry point for `certalert`'s "purge" subcommand."""

    log = logging.getLogger(__name__)
    config = _load_config(fp_config_file=args.config)
    store = CertAlertStore(config=config)

    prefix = args.id
    ca_infos = store.find_ai_by_cert_id_prefix(prefix=prefix)
    ca_info_count = len(ca_infos)
    if ca_info_count > 1:
        do_purge = False
        if not args.force:
            log.warning(f"multiple certificates ({ca_info_count}) match "
                        f"prefix {prefix} (the argument value for 'id'); "
                        "asking if user wants to purge them all")
            confirm = input(f"{ca_info_count} matches for prefix "
                            f"{prefix}; do you want to purge them all "
                            "[y/n]? ")
            if confirm.lower() == "y":
                log.info(f"user confirms to purge all {ca_info_count} "
                         "certificate info states")
                do_purge = True
            else:
                log.info(f"user does aborts purging of {ca_info_count} "
                         "certificate info states")
        else:
            log.info(f"multiple certificates ({ca_info_count}) match "
                     f"prefix {prefix} (the argument value for 'id') "
                     "and --force is set, purging all certificate info "
                     "states")
            do_purge = True
        if do_purge:
            c_ids = [ai.cert_id for ai in ca_infos]
            store.purge_certificates(cert_ids=c_ids)
    elif len(ca_infos) == 1:
        c_id = ca_infos[0].cert_id
        store.purge_certificate(cert_id=c_id)
    elif len(ca_infos) == 0:
        if len(prefix) < 3:
            log.warning(f"prefix {prefix} for cert_id is too short, must "
                        "be at least three characters")
        else:
            log.info(f"no certificate matches prefix {prefix} "
                     "(your argument value for 'id')")


def test_email(args: argparse.Namespace) -> None:
    """Entry point for `certalert`'s "test-email" subcommand."""

    log = logging.getLogger(__name__)
    config = _load_config(fp_config_file=args.config)
    store = CertAlertStore(config=config)

    store.read_only = True  # safety measure
    valid_types = [
        "HEADS_UP",
        "NOTIFY",
        "WARNING",
        "ALERT",
        "POST_MORTEM",
    ]
    typ = args.notification_type.upper()
    if typ not in valid_types:
        log.warning(f"unknown `--notification-type`: {typ}, using NOTIFY")
        typ = "NOTIFY"
    log.info(f"sending a '{typ}' test notification (test-email)")
    recipient = args.recipient
    store.prepare_dummy_notification(recipient_email=recipient,
                                     n_type=typ)
    store.send_notifications()


def _load_config(fp_config_file: str) -> dict:
    """Read the file referenced by `fp_config_file` into a dict.

    Read the file pointed to by `fp_config_file` into a dict, assuming
    that the file is a certalert YAML config file. The dict will contain
    """
    # read the config file (YAML) indicated via the `--config` command
    # line flag and produce the respective Python objects
    # (e.g. Recipients)
    log = logging.getLogger(__name__)
    conf_path = Path(fp_config_file)
    try:
        with open(conf_path, "r") as fh:
            log.info(f"loading config from '{conf_path}'")
            yaml = YAML(typ="safe")
            cfg = yaml.load(fh)

            # because we want clear names in the YAML but short names
            # in code, map `notification_recipients` (in YAML) to
            # `recipients` (in code)
            try:
                cfg["recipients"] = cfg["notification_recipients"]
                del cfg["notification_recipients"]
            except KeyError:
                # mainly when `notification_recipients` is missing in
                # the YAML file, we want to use the hardcoded config
                # which already has `recipients` (not
                # `notification_recipients`)
                pass

            # merge: overwrite existing keys in `config` with values from `cfg`
            config.update(cfg)
    except FileNotFoundError:
        msg = f"unable to find config YAML '{conf_path}', using defualt config"
        log.warning(msg)

    # for using pprint with logging:
    # https://stackoverflow.com/a/11093247
    log.debug("config for this run:")
    log.debug(pprint.pformat(config))

    return config


__MAJOR__ = 2
__MINOR__ = 3
__PATCH__ = 1
__VERSION__ = f"{__MAJOR__}.{__MINOR__}.{__PATCH__}"

# only output these fields/attributes when returning JSON from commands
AI_JSON_FIELDS = ["cert_id",
                  "cert_name",
                  "cert_origin",
                  "cert_source",
                  "valid_from",
                  "valid_until",
                  "ignored",
                  "notification_count",
                  "last_notification"]

# defines the defaults for all (required) config values
# will be overwritten by config loaded from YAML file
config = {
    "certalert_db_path": "certalert.json",
    "recipients": [],
    "notification_schemes": [
        {
            "name": "default",
            "heads_up_days": 90,
            "notification_threshold": 30,
            "notification_frequency": 5,
            "warning_threshold": 14,
            "warning_frequency": 2,
            "alert_threshold": 7,
            "alert_frequency": 1,
            "post_mortem_duration": 30,
        },
    ],
    "dirs_notification_templates_email": [],
    "dirs_pem": [],
    "dirs_index": [],
    "urls": [
        {
            "domain": "expired.badssl.com",
        },
    ],
    "url_conn_timeout": 10,
    "smtp_server": {
        "host": "localhost",
        "port": 25,
        "username": None,
        "password": None,
        "sender": "certalert@localhost",
    },
    "certificates": [],
}

log = None
log_level = logging.INFO  # default log level (can be changed using "-v")
log_conf = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default-console": {
            # exact format is not important, this is the minimum information
            "format": "%(name)-.128s [%(levelname)-8s] %(message)s",
        },
        "default-file": {
            # exact format is not important, this is the minimum information
            "format": "%(asctime)s %(name)-.128s %(levelname)-8s %(message)s",
        },
    },
    "handlers": {
        # console logs to stderr
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "default-console",
        },
        "file": {
            "level": logging.DEBUG,
            "class": "logging.FileHandler",
            "filename": "certalert.log",
            "formatter": "default-file",
        },
    },
    "loggers": {
        "": {  # root logger
            "level": logging.WARNING,
            "handlers": ["console", "file"],
        },
        # Our application code
        "certalert.cert_loader": {
            "level": log_level,
        },
        "__main__": {
            "level": log_level,
        },
        "certalert.certalert_cert": {
            "level": log_level,
        },
        "certalert.certalert_info": {
            "level": log_level,
        },
        "certalert.certalert_store": {
            "level": log_level,
        },
        "certalert.notification": {
            "level": log_level,
        },
        "certalert.notification_scheme": {
            "level": log_level,
        },
        "certalert.recipient": {
            "level": log_level,
        },
    },
}

if __name__ == "__main__":
    sys.exit(main())
