# -*- coding: utf-8 -*-

"""Implements notification schemes.

Support for different lead times for different certficates/usecases.
"""

import logging
import datetime


class NotificationScheme:
    """Defines when and how often notifiations are sent.

    Requirements for notifications can be different for different
    certificates: because Let's Encrypt certificates only have a
    validity of 90 days, it does not make sense to start alerting 90
    days before the certificate expires. For "manual" certificates
    however, 90 days might be perfectly reasonable.

    `heads_up_days` defines the number of days before expriation when a
    "heads up" notification should be sent (something like "hey, this
    will expire in X days"). Heads up are "single shot".

    `notification_threshold` defines how many days before expiration
    `certalert` will start to send expiration notifications.
    Notifications will be re sent after `notification_frequency` number
    of days have passed since the last notification was sent.

    `warning_threshold` and `warning_frequency` as well as
    `alert_threshold` and `alert_frequency` follow the same logic as
    their `notification_...` counterparts but for warnings and alerts
    respectively.

    `post_mortem_duration` defines the number of days until `certalert`
    stops sending post mortem notifications (every day).
    """
    def __init__(self,
                 name: str,
                 heads_up_days: int = 90,
                 notification_threshold: int = 30,
                 notification_frequency: int = 5,
                 warning_threshold: int = 14,
                 warning_frequency: int = 2,
                 alert_threshold: int = 7,
                 alert_frequency: int = 1,
                 post_mortem_duration: int = 30,
                 **kwargs):
        super().__init__(**kwargs)
        self.log = logging.getLogger(__name__)
        self.name = name
        self.heads_up_days = heads_up_days
        self.notification_threshold = notification_threshold
        self.notification_frequency = notification_frequency
        self.warning_threshold = warning_threshold
        self.warning_frequency = warning_frequency
        self.alert_threshold = alert_threshold
        self.alert_frequency = alert_frequency
        self.post_mortem_duration = post_mortem_duration

    def get_notification_type(self,
                              target_dt: datetime.datetime,
                              reference_dt: datetime.datetime = None) -> str:
        if not reference_dt:
            reference_dt = datetime.datetime.now(datetime.timezone.utc)
        type_ = None
        days_left = (target_dt - reference_dt).days
        if days_left < 0:
            if days_left <= self.post_mortem_duration:
                type_ = "POST_MORTEM"
        else:
            if days_left <= self.heads_up_days:
                if days_left <= self.notification_threshold:
                    if days_left <= self.warning_threshold:
                        if days_left <= self.alert_threshold:
                            type_ = "ALERT"
                        else:
                            # less or equal than warning,
                            # but more than alert
                            type_ = "WARNING"
                    else:
                        # less or equal than notification,
                        # but more than warning
                        type_ = "NOTIFY"
                else:
                    # less or equal than heads_up,
                    # but more than notification
                    type_ = "HEADS_UP"
        return type_

    def __str__(self) -> str:
        """String representation of object."""
        me = (f"{self.name} {self.notification_threshold}/"
              f"{self.warning_threshold}/{self.alert_threshold}")
        return me

    def __repr__(self) -> str:
        return f"({id(self)}) {self.__str__()}"
