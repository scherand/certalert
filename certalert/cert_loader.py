# -*- coding: utf-8 -*-

"""Implements the different certificate loaders.

The 'loaders' are the workhorses of certificate ingestion. Three types
of loaders are to be implemented: PEMLoader to load certificates from
PEM files, URLLoader to load certificates from URLs (i.e. the network)
and IndexLoader to load information from an OpenSSL index.txt file.
"""

import logging
import urllib
import socket
import ssl
import csv
# OpenSSL (= pyopenssl) also has cryptography as a dependency...
# import OpenSSL
import tempfile
import base64
import hashlib
import dateutil

from collections.abc import Iterable
from datetime import datetime
from datetime import timezone
from pathlib import Path
# cryptography is a heavy dependency (requires rust on FreeBSD),
# therefore using `OpenSSL` (pyopenssl) instead
# from cryptography import x509
# from cryptography.hazmat.primitives import hashes

from .certalert_cert import Cert
from .recipient import Recipient


# how to declare a custom exception:
# https://stackoverflow.com/a/1319675
class CertLoadError(Exception):
    """Error raised when a certificate cannot be loaded."""
    pass


class CertLoader:
    """Base class for all certificate loaders."""
    def __init__(self,
                 recipients: Iterable[Recipient],
                 notification_scheme_name: str = "default",
                 email_template_name: str = "default",
                 **kwargs):
        super().__init__(**kwargs)
        self.log = logging.getLogger(__name__)
        self.recipients = recipients
        self.scheme_name = notification_scheme_name
        self.e_template_name = email_template_name

    def load(self) -> Iterable[Cert]:
        """Must be implemented in child class."""
        raise NotImplementedError("`load` must be called on child classes "
                                  "of `CertLoader`")

    def _decoded_cert_to_certalert_cert(self,
                                        pem_data: bytes,
                                        decoded_cert: dict,
                                        origin: str = "unknown",
                                        source: str = "unknown") -> Cert:
        """Create `Cert` object from dict.

        The (undocumented) underscore function `ssl._ssl._test_decode_cert()`
        returns a dict with information about a certificate (loaded from file).
        This dict is turned into a `Cert` object here.
        """
        fp = self.fingerprint_from_pem(pem_data)
        common_name = "unknown_CN"
        subject = decoded_cert.get("subject")
        for comp in subject:
            if comp[0][0] == "commonName":
                common_name = comp[0][1]
                break
        serial = decoded_cert.get("serialNumber")
        try:
            ser = int(decoded_cert.get("serialNumber"))
            serial = f"{hex(ser)} ({ser})"
        except ValueError:
            # most likely serial in hex...
            try:
                ser = int(decoded_cert.get("serialNumber"), 16)
                serial = f"{hex(ser)} ({ser})"
            except ValueError:
                pass
        valid_from = dateutil.parser.parse(decoded_cert.get("notBefore"))
        valid_until = dateutil.parser.parse(decoded_cert.get("notAfter"))
        issuer_tup = []
        for c in decoded_cert.get("issuer"):
            abbrv = c[0][0]
            if c[0][0] == "countryName":
                abbrv = "C"
            elif c[0][0] == "stateOrProvinceName":
                abbrv = "ST"
            elif c[0][0] == "organizationName":
                abbrv = "O"
            elif c[0][0] == "commonName":
                abbrv = "CN"
            issuer_tup.append(f"{abbrv}={c[0][1]}")
        issuer = "/".join(issuer_tup)
        c = Cert(serial=serial,
                 issuer=issuer,
                 fingerprint=fp,
                 name=common_name,
                 origin=origin,
                 source=source,
                 notification_scheme_name=self.scheme_name,
                 email_template_name=self.e_template_name,
                 valid_from_dt=valid_from,
                 valid_until_dt=valid_until,
                 pem_data=pem_data)
        return c

    # def _pem_to_cert(self, pem_data: bytes,
    #                  origin: str = "unknown",
    #                  source: str = "unknown") -> Cert:
    #     """Convert a PEM bytes stream into a `Cert` object."""
    #     cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM,
    #                                            pem_data)
    #     # subject is of type OpenSSL.crypto.X509Name
    #     subject = cert.get_subject()
    #     comps = subject.get_components()
    #     # extract CN from subject's components
    #     common_name = "unknown_CN"
    #     for comp in comps:
    #         if comp[0] == "CN":
    #             common_name = comp[1]
    #             break
    #     ser = cert.get_serial_number()
    #     serial = f"{hex(ser)} ({ser})"
    #     fp = cert.digest('sha256').decode("ascii")
    #     valid_from = datetime.strptime(
    #         cert.get_notBefore().decode("ascii"),
    #         "%Y%m%d%H%M%SZ")
    #     valid_from = valid_from.replace(tzinfo=timezone.utc)
    #     valid_until = datetime.strptime(
    #         cert.get_notAfter().decode("ascii"),
    #         "%Y%m%d%H%M%SZ")
    #     valid_until = valid_until.replace(tzinfo=timezone.utc)
    #     # cert.get_issuer() is of type OpenSSL.crypto.X509Name
    #     issuer_comps = [f"{c[0].decode('ascii')}/{c[1].decode('ascii')}" for
    #                     c in cert.get_issuer().get_components()]
    #     issuer = "=".join(issuer_comps)

    #     # equivalent code when using `cryptography` (https://cryptography.io/)
    #     #
    #     # cert = x509.load_pem_x509_certificate(pem_data)
    #     # get the common name (CN) (oid 2.5.4.3) to use as the
    #     # "name"; note that cert.subject is a x509.Name object
    #     # oid = x509.ObjectIdentifier("2.5.4.3")
    #     # oid value for CN always is a list of length 1
    #     # oid_value = cert.subject.get_attributes_for_oid(oid)
    #     # common_name = oid_value[0].value
    #     # serial = f"{hex(cert.serial_number)} ({cert.serial_number})"
    #     # https://cryptography.io/en/latest/x509/reference/#cryptography.x509.Certificate.fingerprint
    #     # fp_b = cert.fingerprint(hashes.SHA256())
    #     # fp = self._byte_hash_to_str(hash=fp_b)
    #     # valid_from = cert.not_valid_before
    #     # valid_until = cert.not_valid_after
    #     # issuer = cert.issuer
    #     c = Cert(serial=serial,
    #              issuer=issuer,
    #              fingerprint=fp,
    #              name=common_name,
    #              origin=origin,
    #              source=source,
    #              notification_scheme_name=self.scheme_name,
    #              valid_from_dt=valid_from,
    #              valid_until_dt=valid_until,
    #              pem_data=pem_data)
    #     return c

    def fingerprint_from_pem(self, pem_data: bytes, formatted=True) -> str:
        """Calculate SHA256 fingerprint of certificate.

        Given the PEM representation of a certificate, calculate its
        SHA256 fingerprint.
        """
        # remove the BEGIN/END markers
        # skip first `begin_idx` characters; 28 is the begin marker's
        # length
        begin_idx = pem_data.index(
            "-----BEGIN CERTIFICATE-----".encode("utf-8")) + 28
        # Note: end_idx will (intentionally) be negative; it counts the
        # number of characters that should be skipped from the end of
        # `pem_data`
        end_idx = pem_data.index(
            "-----END CERTIFICATE-----".encode("utf-8")) - len(pem_data) - 1
        pem_data = pem_data[begin_idx:end_idx]
        pem_decoded = base64.b64decode(pem_data)
        fp = hashlib.sha256(pem_decoded).hexdigest().upper()
        if formatted:
            fp = ":".join([f"{i}{j}" for i, j in zip(fp[::2], fp[1::2])])
        return fp

    def _byte_hash_to_str(self, hash: bytes) -> str:
        """Convert byte hash to formatted string."""
        s = hash.hex().upper()
        # https://stackoverflow.com/a/70435568
        s = ":".join([f"{i}{j}" for i, j in zip(s[::2], s[1::2])])
        return s


class PEMLoader(CertLoader):
    """Load certificates from all PEM files in a folder."""
    def __init__(self,
                 dir_path: str,
                 **kwargs):
        super().__init__(**kwargs)
        self.dir_path = Path(dir_path)

    def load(self) -> Iterable[Cert]:
        """Create list of `Cert` objects.

        Return a list of `Cert` objects, one for every PEM file in
        `.dir_path`.
        """
        if not self.dir_path.is_dir():
            msg = f"'{self.dir_path}' is not a directory, not loading PEMs"
            self.log.warning(msg)
            return []
        certs = []
        pems = list(self.dir_path.glob("*.pem"))
        self.log.debug(f"loading {len(pems)} PEM files from {self.dir_path}")
        for pem in pems:
            self.log.debug(f"opening PEM file '{pem}'")
            cert = ssl._ssl._test_decode_cert(pem)
            with open(pem, "rb") as fh:
                pem_data = fh.read()
                c = self._decoded_cert_to_certalert_cert(
                    pem_data=pem_data,
                    decoded_cert=cert,
                    origin=self.dir_path,
                    source=pem.name)
                c.add_recipients(self.recipients)
                certs.append(c)
        self.log.info(f"loaded {len(pems)} PEM files from {self.dir_path}")
        return certs


class URLLoader(CertLoader):
    """Load certificate from URL (domain)."""
    def __init__(self,
                 **kwargs):
        super().__init__(**kwargs)

    def load(self,
             url: str,
             port: int = 443,
             timeout: int = 10,
             origin: str = "network") -> Cert:
        """Connect to URL and fetch certificate.

        Timeout is in seconds.
        """
        url_o = urllib.parse.urlparse(url)
        # for "full" URL (e.g. https://www.example.com), netloc is
        # "www.example.com" but for "domain only" (e.g. www.example.com),
        # netloc is empty and we need to use path
        domain = url_o.netloc
        if not url_o.scheme:
            domain = url_o.path
        self.log.debug(f"loading cert for URL '{url}' from '{domain}:{port}'")
        try:
            pem_data = self._cert_from_net_as_pem(domain=domain,
                                                  port=port,
                                                  timeout=timeout)
            with tempfile.NamedTemporaryFile() as pem_file:
                pem_file.write(pem_data)
                pem_file.seek(0)
                cert = ssl._ssl._test_decode_cert(pem_file.name)
            c = self._decoded_cert_to_certalert_cert(
                        pem_data=pem_data,
                        decoded_cert=cert,
                        origin=origin,
                        source=domain)
            c.add_recipients(self.recipients)
            self.log.info(f"loaded cert for '{domain}' from URL '{url}'")
            return c
        except CertLoadError:
            return None

    def _cert_from_net_as_pem(self,
                              domain: str,
                              port: int,
                              timeout: int) -> str:
        """Load certificate data (PEM) from network.

        Access `domain:port` via the network and return the received
        certificate as a PEM string.

        The main reason this is packed into a method is to mock it for
        testing.
        """
        # https://stackoverflow.com/a/53556522
        context = ssl.create_default_context()
        # we want the cert in all cases
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        try:
            self.log.debug(f"opening connection to {domain}:{port}")
            conn = socket.create_connection((domain, port), timeout)
            sock = context.wrap_socket(conn, server_hostname=domain)
            sock.settimeout(timeout)
        except socket.gaierror:
            # invalid hostname?
            msg = (f"failed to fetch certificate for '{domain}', "
                   "invalid domain?")
            self.log.warning(msg)
            raise CertLoadError(msg)
        except ConnectionRefusedError:
            msg = (f"failed to fetch certificate for '{domain}', "
                   "connection refused")
            self.log.warning(msg)
            raise CertLoadError(msg)
        except TimeoutError:
            msg = (f"failed to connect to '{domain}' (timeout: {timeout} "
                   "second(s))")
            self.log.warning(msg)
            raise CertLoadError(msg)
        try:
            der_cert = sock.getpeercert(True)
        finally:
            sock.close()
        pem_data = ssl.DER_cert_to_PEM_cert(der_cert).encode(encoding="ASCII")
        return pem_data


class IndexLoader(CertLoader):
    """Load certificate information from OpenSSL index.txt."""
    def __init__(self,
                 dir_path: str,
                 **kwargs):
        super().__init__(**kwargs)
        self.dir_path = Path(dir_path)

    def load(self) -> Iterable[Cert]:
        """Create list of `Cert` objects.

        Return a list of `Cert` objects, one for every line in every
        *.txt file in `.dir_path`.
        """
        if not self.dir_path.is_dir():
            msg = f"'{self.dir_path}' is not a directory, not loading indexes"
            self.log.warning(msg)
            return []
        certs = []
        idxs = list(self.dir_path.glob("*.txt"))
        self.log.debug(f"loading {len(idxs)} index files from {self.dir_path}")
        # some information about the format of the OpenSSL index.txt
        # files:
        # https://groups.google.com/g/mailing.openssl.users/c/gMRbePiuwV0?pli=1
        # datetime format: yymmddHHMMSSZ
        # fields:
        # * 0) Entry type. May be "V" (valid), "R" (revoked) or "E" (expired).
        # * 1) Expiration datetime.
        # * 2) Revokation datetime. This is set for any entry of the type "R".
        # * 3) Serial number.
        # * 4) File name of the certificate. This doesn't seem to be used,
        #      ever, so it's always "unknown".
        # * 5) Certificate subject name.
        headers = [
            "entry_type",
            "expiration_dt_s",
            "revocation_dt_s",
            "serial",
            "file_name",
            "subject",
        ]
        for idx_file in idxs:
            with open(idx_file, "r", newline="") as fh:
                reader = csv.DictReader(fh,
                                        delimiter="\t",
                                        fieldnames=headers)
                for row in reader:
                    if row.get("entry_type") == "V":
                        self.log.debug(row)
                        exp_s = row.get("expiration_dt_s")
                        valid_until = datetime.strptime(exp_s, "%y%m%d%H%M%SZ")
                        valid_until = valid_until.replace(tzinfo=timezone.utc)
                        self.log.debug(f"valid_until: {valid_until}")
                        cert = Cert(serial=row.get("serial"),
                                    issuer=(f"issuer_of_{idx_file}"),
                                    fingerprint=None,
                                    name=row.get("subject"),
                                    origin=self.dir_path,
                                    source=idx_file,
                                    notification_scheme_name=self.scheme_name,
                                    valid_until_dt=valid_until)
                        cert.add_recipients(self.recipients)
                        certs.append(cert)
                    else:
                        msg = ("not loading certificate of type "
                               f"{row.get('entry_type')} with subject "
                               f"'{row.get('subject')}'")
                        self.log.debug(msg)
        return certs
