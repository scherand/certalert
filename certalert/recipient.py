# -*- coding: utf-8 -*-

"""A recipient of notifications from certalert.

Mainly an email address for now. In future, notifications could also
be sent via the messaging service provided by Threema.
"""

from marshmallow import Schema
from marshmallow import fields
from marshmallow import post_load


class Recipient:
    """An entity that recieves notifications from certalert.

    Every recipient has a `name`. In addition, it can have any
    combination of `email` and/or `threema_id`. Notifications will be
    sent to **all** available "targets" (email, threema_ids).

    Recipients which are flagged as `default` will receive notfications
    for all certificates that do not have an individual policy set up.
    """
    def __init__(self,
                 name: str,
                 email: str = None,
                 threema_id: str = None,
                 is_default: bool = False,
                 **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.email = email
        self.threema_id = threema_id
        self.is_default = is_default

    def __str__(self) -> str:
        """String representation of object."""
        me = f"{self.name}"
        if self.is_default:
            me = f"!{me}"
        if self.email:
            me = f"{me} <{self.email}>"
        if self.threema_id:
            me = f"{me} [{self.threema_id}]"
        return me

    def __repr__(self) -> str:
        return f"({id(self)}) {self.__str__()}"


class RecipientSchema(Schema):
    """Marshmallow schema for Recipient objects."""
    name = fields.Str()
    email = fields.Str()
    threema_id = fields.Str(load_default=None)
    is_default = fields.Bool()

    @post_load
    def make_recipient(self, data, **kwargs) -> Recipient:
        """(Re)create object from dict."""
        return Recipient(**data)
