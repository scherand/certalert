# -*- coding: utf-8 -*-

"""Tests for the `Cert` class."""

import logging
import datetime
import dateutil.parser

from collections.abc import Iterable

from .recipient import Recipient


class Cert:
    """Represents a single certficate.

    These are "transient" objects (not persisted to the database), they
    are (re)created for every run of the `certalert` script.

    When creating a `Cert` object, it is important to either provide a
    fingerprint or a serial/issuer combo. This information is used to
    make up the `.cert_id` which is, internally, the unique identifier
    of the cert.
    """
    def __init__(self,
                 serial: str,
                 issuer: str,
                 fingerprint: str,
                 name: str,
                 origin: str,
                 source: str,
                 notification_scheme_name: str = "default",
                 email_template_name: str = "default",
                 valid_from_str: str = None,
                 valid_until_str: str = None,
                 valid_from_dt: datetime.datetime = None,
                 valid_until_dt: datetime.datetime = None,
                 pem_data: str = None,
                 **kwargs):
        super().__init__(**kwargs)
        self.log = logging.getLogger(__name__)
        self.serial = serial
        self.issuer = issuer
        self.fingerprint = fingerprint
        self.name = name
        self.origin = origin
        self.source = source
        self.notification_scheme_name = notification_scheme_name
        self.email_template_name = email_template_name
        self.valid_from_str = valid_from_str
        self.valid_until_str = valid_until_str
        self.pem_data = pem_data
        self.recipients = []

        if not valid_from_dt:
            try:
                valid_from_dt = dateutil.parser.parse(valid_from_str)
            except (ValueError, TypeError):
                valid_from_dt = datetime.datetime(year=1970,
                                                  month=1,
                                                  day=1,
                                                  hour=0,
                                                  minute=0,
                                                  second=1,
                                                  tzinfo=datetime.timezone.utc)
                self.log.debug(valid_from_dt)

        if not valid_until_dt:
            try:
                valid_until_dt = dateutil.parser.parse(valid_until_str)
            except (ValueError, TypeError):
                msg = (f"cannot parse '{valid_until_str}' as datetime, make "
                       "sure to either provide `valid_until_str` or "
                       "`valid_until_dt`")
                raise RuntimeError(msg)

        if not valid_from_dt.tzinfo:
            # dt is naive, make it aware (UTC)
            valid_from_dt = valid_from_dt.replace(
                tzinfo=datetime.timezone.utc)
        else:
            valid_from_dt = valid_from_dt.astimezone(datetime.timezone.utc)

        if not valid_until_dt.tzinfo:
            # dt is naive, make it aware (UTC)
            valid_until_dt = valid_until_dt.replace(
                tzinfo=datetime.timezone.utc)
        else:
            valid_until_dt = valid_until_dt.astimezone(datetime.timezone.utc)

        self.valid_from_dt = valid_from_dt
        self.valid_until_dt = valid_until_dt

    @property
    def fp(self) -> str:
        """Alias for `.fingerprint`.

        The SHA256 fingerprint of the certificate.
        """
        return self.fingerprint

    @property
    def cert_id(self) -> str:
        """A unique Id for the certificate.

        If the `.fingerprint` property is populated (always the case in
        "real life"), the value of this property is used as the cert_id.
        If no `.fingerprint` is available, the Id is constructed by
        concatenating the certificate's `.issuer` with its `.serial`
        using a double colon ("::").
        If any of `.issuer` or `.serial` is (also) not available, a
        `RuntimeError` is raised.
        """
        if self.fp:
            return self.fp
        else:
            if self.issuer and self.serial:
                return f"{self.issuer}::{self.serial}"
            else:
                msg = ("certificate must either have a fingerprint or "
                       "an issuer/serial combo to build a unique "
                       "`.cert_id`")
                raise RuntimeError(msg)

    @property
    def human_name(self) -> str:
        """A (hopefully) human readable name for this cert."""
        try:
            cert_id_short = self.cert_id[:14]
        except (TypeError, RuntimeError):
            # TypeError: 'NoneType' object is not subscriptable
            cert_id_short = "unknown fingerprint"
        return f"{self.name} [{self.serial}, {cert_id_short}]"

    @property
    def days_left(self, reference_dt: datetime.datetime = None) -> int:
        """Number of days left until certificate expires.

        With regard to reference date (if not provided: "now" is used
        as reference).

        For expired certificates, the number will be negative!
        """
        if not reference_dt:
            reference_dt = datetime.datetime.now(datetime.timezone.utc)
        return (self.valid_until_dt - reference_dt).days

    def add_recipient(self, recipient: Recipient) -> None:
        """Append a recipient to the list of recipients."""
        self.recipients.append(recipient)

    def add_recipients(self, recipients: Iterable[Recipient]) -> None:
        """Append several recipients to the list of recipients."""
        self.recipients.extend(recipients)

    def __str__(self) -> str:
        """String representation."""
        return self.human_name

    def __repr__(self) -> str:
        """Textual representation."""
        return self.__str__()
