# -*- coding: utf-8 -*-

"""Implements the `certalert` info object.

The certalert info object is the main carrier of information in certalert.
"""

import logging
import datetime

from collections.abc import Iterable
from marshmallow import Schema
from marshmallow import fields
from marshmallow import post_load

from .notification import Notification
from .notification import NotificationSchema


class CertAlertInfo:
    """Information about the (past) processing of a cert in `certalert`.

    `certalert` needs to keep track of some "historical" information,
    like when a notification was last sent for example. This class
    implements this "information store".

    `CertAlertInfo` objects are the main part of what is stored in the
    `certalert` database (JSON file).
    """
    def __init__(self,
                 cert_id: str,
                 cert_name: str,
                 cert_origin: str,
                 cert_source: str,
                 valid_from: datetime.datetime,
                 valid_until: datetime.datetime,
                 ignored: bool = False,
                 last_success_dt: datetime.datetime = None,
                 last_fail_dt: datetime.datetime = None,
                 notifications: Iterable[Notification] = None,
                 **kwargs):
        super().__init__(**kwargs)
        self.log = logging.getLogger(__name__)
        self.cert_id = cert_id
        self.cert_name = cert_name
        self.cert_origin = cert_origin
        self.cert_source = cert_source
        self.valid_from = valid_from
        self.valid_until = valid_until
        self.ignored = ignored
        self.last_success_dt = last_success_dt
        self.last_fail_dt = last_fail_dt
        if not notifications:
            self.notifications = []
        else:
            self.notifications = notifications

    @property
    def failed(self) -> bool:
        """Determine if last cert check failed."""
        did_fail = False
        if self.last_fail_dt:
            if ((not self.last_success_dt) or
                    (self.last_fail_dt > self.last_success_dt)):
                did_fail = True
        return did_fail

    @property
    def last_notification(self):
        """The most recent notification (if any)."""
        n_fication = None
        if self.notifications:
            sortd = sorted(self.notifications, key=lambda x: x.created_at)
            n_fication = sortd[-1]
        return n_fication

    @property
    def notification_count(self):
        """The number of notifications sent until now."""
        return len(self.notifications)

    def add_notification(self, notification: Notification) -> None:
        """Add a notification to the list of notifications."""
        self.notifications.append(notification)

    def __str__(self) -> str:
        """String representation."""
        if self.last_notification:
            last_type = self.last_notification.notify_type
        else:
            last_type = "n/a"
        if self.ignored:
            disbl_indicator = "*"
        else:
            disbl_indicator = " "
        msg = (f"{disbl_indicator}{self.cert_name}, valid until "
               f"{self.valid_until}, {len(self.notifications)} "
               "notification(s), last processed "
               f"notification ({last_type}): {self.last_success_dt}")
        return(msg)

    def __repr__(self) -> str:
        """Textual representation."""
        return self.__str__()


class CertAlertInfoSchema(Schema):
    """Marshmallow schema for CertAlertInfo objects."""
    # use different keys in object vs JSON:
    # https://marshmallow.readthedocs.io/en/stable/quickstart.html#specifying-serialization-deserialization-keys
    cert_id = fields.Str()
    cert_name = fields.Str()
    cert_origin = fields.Str()
    cert_source = fields.Str()
    valid_from = fields.AwareDateTime(data_key="not_before", load_default=None)
    valid_until = fields.AwareDateTime(data_key="not_after")
    ignored = fields.Boolean()
    last_success_dt = fields.AwareDateTime(load_default=None)
    last_fail_dt = fields.AwareDateTime(load_default=None)
    notifications = fields.Method("dump_notifications",
                                  deserialize="load_notifications")
    notification_count = fields.Int(dump_only=True)
    last_notification = fields.Method("dump_last_notification", dump_only=True)

    @post_load
    def make_cert_alert_info(self, data, **kwargs) -> CertAlertInfo:
        """(Re)create object from dict."""
        return CertAlertInfo(**data)

    # The method used with `fields.Method` must take a single argument
    # which is the object to serialise:
    # https://marshmallow.readthedocs.io/en/stable/custom_fields.html#method-fields
    # To provide additional information to the method, use `context`:
    # https://marshmallow.readthedocs.io/en/stable/custom_fields.html#adding-context-to-method-and-function-fields
    def dump_notifications(self, ai) -> dict:
        """Dump/serialise all `Notification`s."""
        n_schema = NotificationSchema(many=True)
        return n_schema.dump(ai.notifications)

    def load_notifications(self, data) -> Iterable[Notification]:
        """Deserialize list of notifications"""
        n_schema = NotificationSchema(many=True)
        return n_schema.load(data)

    def dump_last_notification(self, ai) -> dict:
        """Dump/serialise the last `Notification`."""
        ln = ai.last_notification
        if not ln:
            return {}
        fields = ["created_at", "notify_type", "sent", "failed"]
        n_schema = NotificationSchema(only=fields)
        return n_schema.dump(ln)
