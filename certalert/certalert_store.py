# -*- coding: utf-8 -*-

"""Implements the `certalert` store.

The 'store' is the central entity holding all the information about
known certificates as well as notifications that have been sent in the
past.
"""

import logging
import datetime
import dateutil.parser
import smtplib
import ssl
import json
import tempfile
import shutil
import socket

from collections.abc import Iterable
from pathlib import Path

from .recipient import Recipient
from .notification import Notification
from .notification_scheme import NotificationScheme
from .certalert_cert import Cert
from .certalert_info import CertAlertInfo
from .certalert_info import CertAlertInfoSchema
from .cert_loader import PEMLoader
from .cert_loader import URLLoader
from .cert_loader import IndexLoader


class CertAlertStore:
    """
    The "entry point" for interaction with the `certalert` database.

    Whenever code want's to interact with the `certalert` database, it
    should use the methods provided by `CertAlertStore`.
    """
    # The version of the JSON "database" this release of `certalert` uses
    __DB_VERSION__ = "1.1.0"
    # List of all version of the `certalert` "database" in existence
    __ALL_DB_VERSIONS__ = ["1.0.0", "1.1.0"]
    # Dummy datetime that is used when a required datetime is unknown
    __DUMMY_DT__ = datetime.datetime(year=1970,
                                     month=1,
                                     day=1,
                                     tzinfo=datetime.timezone.utc)

    def __init__(self,
                 config: dict,
                 read_only: bool = False,
                 **kwargs):
        super().__init__(**kwargs)
        self.log = logging.getLogger(__name__)
        self.config = config
        self._db_migration_steps = []
        self.recipients = self._load_recipients(config=config)
        self.notification_schemes = self._load_notification_schemes(
            config=config)
        self.email_templates = self._load_notification_templates_email(
            config=config)
        self.read_only = read_only
        self.certificates = []
        self.alert_infos = []
        self._init_from_db()
        self._notifications = []

    @property
    def ignored_cert_ids(self) -> Iterable[str]:
        """List of all cert_ids that are currently ignored."""
        ignored = set()
        for ai in self.alert_infos:
            if ai.ignored:
                ignored.add(ai.cert_id)
        return list(ignored)

    def prepare_notifications(self) -> Iterable[Notification]:
        """Check if notifications must be generated for known certs."""
        # just to be sure; the call to `_ensure_ai_for_certs()` should
        # not be necessary
        self._ensure_ai_for_certs()
        now = datetime.datetime.now(datetime.timezone.utc)
        for c in self.certificates:
            # ai must be found, because `._ensure_ai_for_certs()` was
            # just called
            ai = self._get_alert_info(cert_id=c.cert_id)
            if ai.ignored:
                self.log.debug("skipping notification check for ignored "
                               f"certificate {ai.cert_name}")
                continue
            n_scheme = self._get_notification_scheme(
                name=c.notification_scheme_name)
            if not n_scheme:
                msg = ("cannot find notification scheme with name "
                       f"'{c.notification_scheme_name}'")
                raise RuntimeError(msg)
            n_type = n_scheme.get_notification_type(target_dt=c.valid_until_dt)
            self.log.debug(f"certificate {c.human_name} is in notification "
                           f"state '{n_type}'")
            if not n_type:
                # no need for notification
                continue

            ai.last_success_dt = now

            need_notify = False
            n_prev = ai.last_notification
            if n_prev:
                days_passed = (now - n_prev.created_at).days
                hysteresis = 1000000  # dummy value
                if n_type == "NOTIFY":
                    hysteresis = n_scheme.notification_frequency
                elif n_type == "WARNING":
                    hysteresis = n_scheme.warning_frequency
                elif n_type == "ALERT":
                    hysteresis = n_scheme.alert_frequency
                elif n_type == "POST_MORTEM":
                    if not n_prev.is_sent:
                        msg = ("unsent POST_MORTEM notification for "
                               f"{c.human_name}, creating another "
                               "POST_MORTEM notification")
                        self.log.warning(msg)
                    else:
                        msg = ("POST_MORTEM notification for "
                               f"{c.human_name} already sent, not creating "
                               "another notification")
                        self.log.debug(msg)
                        # hack to make the following `if` fail
                        days_passed = hysteresis - 1
                if days_passed >= hysteresis:
                    need_notify = True
            elif n_type == "POST_MORTEM":
                pm_dur = n_scheme.post_mortem_duration
                if abs(c.days_left) <= pm_dur:
                    need_notify = True
                else:
                    msg = ("never sent a POST_MORTEM notification for "
                           f"{c.human_name} but cert expired more than "
                           f"{pm_dur} days ago ({abs(c.days_left)} days), "
                           "not sending notification")
                    self.log.info(msg)
            else:
                need_notify = True

            if need_notify:
                msg = (f"creating a notification of type {n_type} "
                       f"for cert {c.human_name}")
                self.log.info(msg)
                n = self._create_notification(cert=c, notify_type=n_type)
                ai.add_notification(n)
                self._notifications.append(n)
        self.log.info(f"prepared {len(self._notifications)} notifications")
        return self._notifications

    def prepare_dummy_notification(self,
                                   recipient_email: str,
                                   n_type: str = "NOTIFY") -> Notification:
        """Create a dummy notification.

        Used for testing SMTP config.
        """
        self.log.debug(f"preparing a dummy notification for {recipient_email}")
        r = Recipient(name=recipient_email,
                      email=recipient_email)
        now = datetime.datetime.now(datetime.timezone.utc)
        day_1 = datetime.timedelta(days=1)
        then = now - day_1
        tomorrow = now + day_1
        c = Cert(serial="DU:MY",
                 issuer="certalert test notification sender",
                 fingerprint="NO:TA:RE:AL:CE:RT",
                 name="not.a.real.certificate",
                 origin="certalert_test_sender",
                 source="allmadeup",
                 valid_from_dt=then,
                 valid_until_dt=tomorrow)
        c.add_recipient(r)
        n = self._create_notification(cert=c, notify_type=n_type)
        self._notifications.append(n)
        self.log.info(f"prepared a dummy notification for {recipient_email}")
        return n

    def send_notifications(self) -> None:
        """Send all pending notifications."""
        if len(self._notifications) == 0:
            self.log.info("no notifications to send, skipping")
            # still persist data as there might be new AlertInfo objects
            self._persist()
            return
        smtp_conf = self.config.get("smtp_server")
        smtp_server = smtp_conf.get("host")
        smtp_port = smtp_conf.get("port")
        sender = smtp_conf.get("sender")
        username = smtp_conf.get("username")
        password = smtp_conf.get("password")
        self.log.info(f"connecting to SMTP server {smtp_server}:{smtp_port}")
        # about handling the exception that can happen when trying to
        # connect to the SMTP server
        # https://stackoverflow.com/a/5205878
        smtp_timeout = 30
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        try:
            server = smtplib.SMTP(smtp_server,
                                  smtp_port,
                                  timeout=smtp_timeout)
            server.ehlo()  # necessary?
            # just calling `.starttls()` here is NOT really secure in an
            # unfriendly environment, it is just the optimistic approach
            # https://stackoverflow.com/a/33891521
            server.starttls(context=context)
            # documentation states:
            # > You should then call ehlo() again
            # "then" meaning after calling `.starttls()`
            # https://docs.python.org/3.10/library/smtplib.html#smtplib.SMTP.starttls
            server.ehlo()
        except (ConnectionRefusedError,
                smtplib.SMTPServerDisconnected,
                socket.gaierror) as e:
            msg = ("failed to connect to SMTP server "
                   f"({smtp_server}:{smtp_port}): {e}")
            self.log.warning(msg)
            self.log.warning("not sending notifications!")
            self._set_notification_failed()
        except TimeoutError:
            msg = (f"connect to SMTP server ({smtp_server}:{smtp_port}) "
                   "timed out (timeout {smtp_timeout} second(s))")
            self.log.warning(msg)
            self.log.warning("not sending notifications!")
            self._set_notification_failed()
        else:
            with server:
                # about using SSL (starttls) with SMTP:
                # https://stackoverflow.com/q/33857698
                # https://stackoverflow.com/a/64890
                # context = ssl.create_default_context()
                # server.starttls(context)
                if username:
                    try:
                        server.login(username, password)
                    except smtplib.SMTPAuthenticationError as e:
                        msg = ("failed to login to SMTP server "
                               f"({smtp_server}:{smtp_port}): {e}")
                        self.log.warning(msg)
                        self.log.warning("not sending notifications!")
                        self._set_notification_failed()
                        self._persist()
                        return
                for n in sorted(self._notifications, key=lambda x: x.priority):
                    self.log.info(f"sending notification {n}")
                    for r in n.recipients:
                        msg = (f"sending notification to recipient {r}")
                        self.log.info(msg)
                        eml_msg = self._notification_to_eml(sender=sender,
                                                            recipient=r.email,
                                                            notification=n)
                        try:
                            send_dt = datetime.datetime.now(
                                datetime.timezone.utc)
                            server.sendmail(sender, r.email, eml_msg)
                            n.sent = send_dt
                        except smtplib.SMTPSenderRefused as e:
                            msg = (f"failed to send notification {n}: "
                                   f"{e}")
                            self.log.warning(msg)
                            n.failed = send_dt
        self._persist()

    def clear_notifications(self) -> None:
        """Remove all (pending) notifications.

        This is basically the inverse of `.prepare_notifications()`.
        """
        self._notifications = []

    def load(self, load_from_config: bool = False) -> None:
        """Fetch all available certificates from source.

        This populates the `.certificates` property but does not
        process any certificate or send any notification.

        If the config contains a "certificates" key/section, these
        certificates are also loaded.
        """
        hardcoded = self.config.get("certificates")
        if load_from_config and hardcoded:
            self.log.debug(f"loading {len(hardcoded)} certificates from YAML "
                           "config file")
            for c in hardcoded:
                rs = self._recipients_from_config(config=c)
                if c.get("recipients"):
                    del c["recipients"]
                new_cert = Cert(**c)
                new_cert.add_recipients(recipients=rs)
                self.certificates.append(new_cert)
            self.log.info(f"loaded {len(hardcoded)} certificates from YAML "
                          "config file")
        dirs_pem = self.config.get("dirs_pem")
        if dirs_pem:
            self.log.debug(f"loading {len(dirs_pem)} directories of PEM files")
            for d in dirs_pem:
                rs = self._recipients_from_config(config=d)
                scheme_name = d.get("notification_scheme", "default")
                e_template_name = d.get("templates_email", "default")
                loader = PEMLoader(dir_path=d.get("dir"),
                                   notification_scheme_name=scheme_name,
                                   email_template_name=e_template_name,
                                   recipients=rs)
                certs = loader.load()
                self.certificates.extend(certs)
            self.log.info(f"loaded {len(dirs_pem)} directories of PEM files")
        dirs_index = self.config.get("dirs_index")
        if dirs_index:
            self.log.debug(f"loading {len(dirs_index)} directories of "
                           "*.txt (index) files")
            for d in dirs_index:
                rs = self._recipients_from_config(config=d)
                scheme_name = d.get("notification_scheme", "default")
                e_template_name = d.get("templates_email", "default")
                loader = IndexLoader(dir_path=d.get("dir"),
                                     notification_scheme_name=scheme_name,
                                     email_template_name=e_template_name,
                                     recipients=rs)
                certs = loader.load()
                self.certificates.extend(certs)
            self.log.info(f"loaded {len(dirs_index)} directories of *.txt "
                          "(index) files")
        urls = self.config.get("urls")
        if urls:
            self.log.debug(f"loading {len(urls)} certificates from URL")
            for url in urls:
                rs = self._recipients_from_config(config=url)
                scheme_name = url.get("notification_scheme", "default")
                e_template_name = url.get("templates_email", "default")
                loader = URLLoader(notification_scheme_name=scheme_name,
                                   email_template_name=e_template_name,
                                   recipients=rs)
                timeout_s = url.get("timeout",
                                    self.config.get("url_conn_timeout", 30))
                cert = loader.load(url=url.get("domain"),
                                   port=url.get("port", 443),
                                   timeout=timeout_s)
                if cert:
                    self.certificates.append(cert)
                else:
                    msg = f"no cert loaded for '{url.get('domain')}'"
                    self.log.warning(msg)
            self.log.info(f"loaded {len(urls)} certificates from URL")
        self._ensure_ai_for_certs()

    def find_ai_by_cert_id_prefix(self,
                                  prefix: str) -> Iterable[CertAlertInfo]:
        """Find `CertAlertInfo` objects of certificates.

        Returns a list of `CertAlertInfo` objects for all certificates
        that have an Id that matches `prefix`.

        The prefix must at least be three characters long.
        """
        matching = []

        if len(prefix) < 3:
            self.log.debug(f"search prefix '{prefix}' is too short "
                           "(< 3 characters)")
            return matching

        self.log.debug(f"looking for certificate informaion about "
                       f"certificate(s) with an Id that starts with {prefix}")
        # list comprehension could be used here, but it does not make
        # the code more readable (in fact, rather less readable)
        for ai in self.alert_infos:
            if ai.cert_id.lower().startswith(prefix.lower()):
                matching.append(ai)

        return matching

    def ignore_cert(self, cert_id: str) -> None:
        """Set cert with Id as ignored."""
        ai = self.find_ai_by_cert_id_prefix(prefix=cert_id)
        if len(ai) == 1:
            ai = ai[0]
        elif len(ai) > 1:
            self.log.warning(f"ambigous cert Id {cert_id}, cannot ignore it")
            return
        if ai:
            if ai.ignored:
                self.log.warning(f"certificate with Id {cert_id} is already "
                                 "ignored")
                return
            else:
                self.log.info(f"ignoring certificate with Id {cert_id} from "
                              "now on")
                ai.ignored = True
                self._persist()
        else:
            self.log.warning(f"no cert with Id {cert_id} in store, cannot "
                             "ignore it")

    def unignore_cert(self, cert_id: str) -> None:
        """Set cert with Id as not ignored."""
        ai = self.find_ai_by_cert_id_prefix(prefix=cert_id)
        if len(ai) == 1:
            ai = ai[0]
        elif len(ai) > 1:
            self.log.warning(f"ambigous cert Id {cert_id}, cannot unignore it")
            return
        if ai:
            if ai.ignored:
                self.log.info(f"unignoring certificate with Id {cert_id} from "
                              "now on")
                ai.ignored = False
                self._persist()
            else:
                self.log.warning(f"certificate with Id {cert_id} is not "
                                 "ignored")
                return
        else:
            self.log.warning(f"no cert with Id {cert_id} in store, cannot "
                             "unignore it")

    def purge_certificates(self, cert_ids: Iterable[str]) -> None:
        """Remove information about multiple certificates.

        This is "duplicated" from "single certificate purging" to
        avoid persisting the data every time after an alert info is
        purged.
        """
        for c_id in cert_ids:
            self.purge_certificate(cert_id=c_id, persist=False)
        self._persist()

    def purge_certificate(self, cert_id: str, persist: bool = True) -> None:
        """Remove information about a certificate.

        Delete all the "history" about the certificate with Id
        `.cert_id`. Internally this means: delete the cert's
        `CertAlertInfo`.

        Note that this will **not** remove a certificate's source (PEM,
        index.txt or URL in config). If the source prevails, a new
        `CertAlertInfo` object will be created during `certalert`s next
        `--notify` run!
        """
        ai = self._get_alert_info(cert_id=cert_id)
        if not ai:
            self.log.debug(f"no alert info for cert {cert_id} in db, cannot "
                           "purge")
        else:
            self.log.info("purging certificate info for certificate "
                          f"{cert_id}")
            self.alert_infos.remove(ai)
            if persist:
                self._persist()

    def _ensure_ai_for_certs(self) -> None:
        """Ensure store has an `CertAlertInfo` object for every cert."""
        self._migrate_db_if_required()
        for c in self.certificates:
            ai = self._get_alert_info(cert_id=c.cert_id)
            if not ai:
                self.log.debug(f"no alert info for cert {c.human_name} in db "
                               "yet, creating one")
                ai = CertAlertInfo(cert_id=c.cert_id,
                                   cert_name=c.human_name,
                                   cert_origin=c.origin,
                                   cert_source=c.source,
                                   valid_from=c.valid_from_dt,
                                   valid_until=c.valid_until_dt)
                self.alert_infos.append(ai)

    def _create_notification(self,
                             cert: Cert,
                             notify_type: str) -> Notification:
        tmpls = self.email_templates.get(cert.email_template_name)
        tmpl = tmpls.get(notify_type.lower())
        if not tmpl:
            tmpl = tmpls.get("default")
        msg = tmpl.get("body")
        subject = tmpl.get("subject")
        # replace placeholders in template
        body = self._replace_placeholder_tokens(cert=cert,
                                                text=msg,
                                                notify_type=notify_type)
        subject = self._replace_placeholder_tokens(cert=cert,
                                                   text=subject,
                                                   notify_type=notify_type)
        priority = 0
        if notify_type == "HEADS_UP":
            priority += 5000
        elif notify_type == "NOTIFY":
            priority += 4000
        elif notify_type == "WARNING":
            priority += 3000
        elif notify_type == "ALERT":
            priority += 2000
        elif notify_type == "POST_MORTEM":
            priority += 1000
        else:
            priority = 10000
        priority += abs(cert.days_left)
        n = Notification(cert_id=cert.cert_id,
                         cert_name=cert.human_name,
                         notify_type=notify_type,
                         priority=priority,
                         recipients=cert.recipients,
                         subject=subject,
                         message=body)
        return n

    def _load_recipients(self, config: dict) -> Iterable[Recipient]:
        """Convert dict to list of objects.

        The result of the loading of the YAML config file is a dict;
        this dict should now be converted into a list of Python objects
        (of type `Recipient`).
        """
        recipients = []
        for recipient in config["recipients"]:
            r = Recipient(name=recipient.get("name"),
                          email=recipient.get("email"),
                          threema_id=recipient.get("threema_id"),
                          is_default=recipient.get("default"))
            recipients.append(r)
        return recipients

    def _load_notification_schemes(
            self, config: dict) -> Iterable[NotificationScheme]:
        """Convert dict to list of objects.

        The result of the loading of the YAML config file is a dict;
        this dict should now be converted into a list of Python objects
        (of type `NotificationScheme`).
        """
        notify_schemes = []
        for notify_scheme in config["notification_schemes"]:
            s = NotificationScheme(**notify_scheme)
            notify_schemes.append(s)
        return notify_schemes

    def _load_notification_templates_email(
            self, config: dict) -> Iterable[str]:
        """Load email template(s) from file.

        Template texts for email notifications (one for each
        notification type) can be stored in files on disk. These files
        are read here and the text they contain is stored in a dict
        with the notification type as the key.

        If no template is found, a default text is used.
        """
        email_templates = {}
        has_default = False
        for d in config.get("dirs_notification_templates_email"):
            dir_name = d.get("dir").lower()
            default = d.get("default")
            dir_path = Path(dir_name)
            if not dir_path.is_dir():
                msg = f"'{dir_path}' is not a directory, not loading templates"
                self.log.warning(msg)
            else:
                dir_templates = email_templates.setdefault(dir_name, {})
                tmpls = list(dir_path.glob("*.txt"))
                self.log.debug(f"loading {len(tmpls)} templates from "
                               "directory '{dir_path}'")
                for tmpl in tmpls:
                    self.log.debug(f"loading email template '{tmpl}'")
                    body = ""
                    with open(tmpl, "r") as fh:
                        for line in fh:
                            line = line.strip()
                            if line.lower().startswith("subject:"):
                                subject = line[8:]
                            else:
                                body = f"{body}\n{line}"
                        tmpl_data = {
                            "subject": subject,
                            "body": body,
                        }
                        template_key = tmpl.stem.lower()
                        self.log.debug(f"adding template as '{template_key}' "
                                       f"to templates from '{dir_name}'")
                        dir_templates[template_key] = tmpl_data
                    if default:
                        if has_default:
                            msg = (f"{dir_name} is not the first template "
                                   "directory marked as default; there can "
                                   "only be one default directory, ignoring "
                                   f"default flag for {dir_name}")
                            self.log.warning(msg)
                        else:
                            default_templates = email_templates.setdefault(
                                "default", {})
                            template_key = tmpl.stem.lower()
                            self.log.debug("adding template as "
                                           f"'{template_key}' to default "
                                           "templates")
                            default_templates[template_key] = tmpl_data
            if default:
                has_default = True
        default_templates = email_templates.setdefault("default", {})
        if not default_templates:
            default = {
                "subject": DEFAULT_EMAIL_TEMPLATE_SUBJECT,
                "body": DEFAULT_EMAIL_TEMPLATE_BODY,
            }
            default_templates["default"] = default
        return email_templates

    def _get_certificate(self, cert_id: str) -> Cert:
        """Find certificate in list of certificates."""
        cert = None
        for c in self.certificates:
            if c.cert_id == cert_id:
                cert = c
                break
        return cert

    def _get_alert_info(self, cert_id: str) -> CertAlertInfo:
        """Find alert info in list.

        Search the list of alert infos (`.alert_infos`) for an object
        with a matching cert_id.
        """
        ai = None
        for el in self.alert_infos:
            if el.cert_id == cert_id:
                ai = el
                break
        return ai

    def _get_default_recipients(self) -> Iterable[Recipient]:
        """Find all default recipients in list of known recipients."""
        rs = []
        for rec in self.recipients:
            if rec.is_default:
                rs.append(rec)
        return rs

    def _get_recipient(self, name: str) -> Recipient:
        """Find recipient in list of known recipients (by name)."""
        r = None
        for rec in self.recipients:
            if rec.name == name:
                r = rec
                break
        return r

    def _get_notification_scheme(self, name: str) -> NotificationScheme:
        """Find notification scheme in list (by name).

        Search the list of notification schemes (`.notification_schemes`)
        for an object with a matching name.
        """
        ns = None
        for el in self.notification_schemes:
            if el.name == name:
                ns = el
                break
        return ns

    def _recipients_from_config(self, config: dict) -> Iterable[Recipient]:
        """Get the recipient objects.

        For a config element with a key 'recipients', get the recipient
        objects. If no 'recipients' key is in the config element, return
        the default recipients.
        """
        r_names = config.get("recipients")
        if r_names:
            rs = [self._get_recipient(name=n) for n in r_names]
        else:
            rs = self._get_default_recipients()
        return rs

    def _notification_to_eml(self,
                             sender: str,
                             recipient: str,
                             notification: Notification) -> str:
        """Add "boilerplate" to turn message into email."""
        today = datetime.datetime.now(tz=dateutil.tz.gettz("Europe/Zurich"))
        eml = f"""From: {sender}
To: {recipient}
Date: {today.strftime('%a, %d %b %Y %H:%M:%S %z')}
Subject: {notification.subject}

{notification.message}"""
        return eml

    def _replace_placeholder_tokens(self,
                                    cert: Cert,
                                    text: str,
                                    notify_type: str) -> str:
        """Replace placeholder tokens in text.

        Replace all instances of known placeholder tokens in `text` with
        their "real" values from `cert`.
        """
        msg = text.replace("[notify_type]", notify_type)
        msg = msg.replace("[cert.name]", cert.name)
        msg = msg.replace("[cert.human_name]", cert.human_name)
        msg = msg.replace("[cert.cert_id]", cert.cert_id)
        msg = msg.replace("[cert.not_valid_after]", str(cert.valid_until_dt))
        msg = msg.replace("[cert.days_left]", str(cert.days_left))
        msg = msg.replace("[cert.origin]", str(cert.origin))
        msg = msg.replace("[cert.source]", str(cert.source))
        return msg

    def _set_notification_failed(self) -> None:
        """Set all notifications `failed` timestamp to now."""
        failed_dt = datetime.datetime.now(datetime.timezone.utc)
        for n in sorted(self._notifications, key=lambda x: x.priority):
            msg = f"setting failed timestamp for notification {n}"
            self.log.debug(msg)
            n.failed = failed_dt

    @classmethod
    def _get_db_migration_steps(cls,
                                curr_v: str,
                                target_v: str) -> Iterable[tuple]:
        """Find db version migration steps from "curr" to "target".

        Examples:
        __ALL_DB_VERSIONS__ = ["1.0.0", "1.1.0", "1.2.0", "1.2.1", "1.2.3"]
        dbv = __ALL_DB_VERSIONS__

        >>> list(zip(dbv[0:1], dbv[1:2]))
        [('1.0.0', '1.1.0')]

        >>> list(zip(dbv[0:2], dbv[1:3]))
        [('1.0.0', '1.1.0'), ('1.1.0', '1.2.0')]

        >>> list(zip(dbv[0:2], dbv[1:4]))
        [('1.0.0', '1.1.0'), ('1.1.0', '1.2.0')]

        >>> list(zip(dbv[0:3], dbv[1:4]))
        [('1.0.0', '1.1.0'), ('1.1.0', '1.2.0'), ('1.2.0', '1.2.1')]

        >>> list(zip(dbv[1:4], dbv[2:5]))
        [('1.1.0', '1.2.0'), ('1.2.0', '1.2.1'), ('1.2.1', '1.2.3')]
        """
        try:
            start_idx = cls.__ALL_DB_VERSIONS__.index(curr_v)
        except ValueError:
            msg = (f"'{curr_v}' is not a known database version to migrate "
                   "from, cannot migrate database")
            raise ValueError(msg)
        try:
            end_idx = cls.__ALL_DB_VERSIONS__.index(target_v)
        except ValueError:
            msg = (f"'{target_v}' is not a known database version to migrate "
                   "to, cannot migrate database")
            raise ValueError(msg)
        if start_idx > end_idx:
            msg = (f"'{curr_v}' is more recent than '{target_v}', database "
                   "downgrading is not supported")
            raise ValueError(msg)
        # a list of pairs of db versions to migrate from/to
        dbv = cls.__ALL_DB_VERSIONS__
        return list(zip(dbv[start_idx:end_idx], dbv[start_idx+1:end_idx+1]))

    def _migrate_db_if_required(self, db_data=None,
                                persist=False,
                                backup=False) -> None:
        """If required, transform db to newer version.

        Check `self._db_migration_steps` for migration steps that should
        be done. A migration step is a tuple of str indicating the
        "from" and "to" db version, for example ("1.0.0", "1.1.0").

        Steps that can be done idempotent should remain in the list.
        This function can be called multiple times in the lifetime of
        the store and if a migration is "currently not possible" (for
        example because information is missing, like certs are not yet
        loaded) it might be possible to properly do the migration step
        later.

        Steps that are not idempotent should either not be done or, if
        they are done, removed from the `_db_migration_steps` list.

        The method can either operate on the raw dict loaded from
        storage (triggered when `db_data` is passed in) or on the
        objects in `self.alert_infos` and `self.certificates`.

        If "persist" is True, the database will be persisted right after
        the migration step, if possible. Persisting is not possible for
        example if the first migration step could not yet be done.

        Setting `backup` to `True` will result in a copy of the current
        database with a timestamp in the filename.
        """
        if not self._db_migration_steps:
            self.log.debug("no pending db migration steps")
            if persist:
                self._persist(backup=backup)
            return db_data
        self.log.info("checking if any of the following db migration "
                      f"steps can be done: {self._db_migration_steps}")

        if db_data and (("1.0.0", "1.1.0") in self._db_migration_steps):
            # migrate dict loaded from JSON from v1.0.0 to v1.1.0
            # this is idempotent
            for ai_json in db_data.get("alert_infos"):
                if not ai_json.get("not_after"):
                    # this happens when loading an "1.0.0" database
                    # that does not have `not_before` and `not_after`
                    # dates
                    ai_json["not_after"] = self.__DUMMY_DT__.isoformat()
        elif ("1.0.0", "1.1.0") in self._db_migration_steps:
            # migrating from 1.0.0 to 1.1.0 means:
            # - add cert valid_from/valid_until to AlertInfo if missing
            #
            # this step is idempotent, but as it will produce the same
            # result on future calls, the step is still removed from
            # _db_migration_steps
            updated_ais = set()
            for c in self.certificates:
                ai = self._get_alert_info(cert_id=c.cert_id)
                if not ai:
                    continue
                if not ai.valid_from and c.valid_from_dt:
                    self.log.debug(f"alert info for {ai.cert_name} can be "
                                   "updated with a valid_from date "
                                   f"({c.valid_from_dt})")
                    ai.valid_from = c.valid_from_dt
                    updated_ais.add(ai)
                if (ai.valid_until == self.__DUMMY_DT__) and c.valid_until_dt:
                    self.log.debug(f"alert info for {ai.cert_name} can be "
                                   "updated with a valid_until date "
                                   f"({c.valid_until_dt})")
                    ai.valid_until = c.valid_until_dt
                    updated_ais.add(ai)
            self.log.info("successfully migrated database from v1.0.0 to "
                          f"v1.1.0: updated {len(updated_ais)} AlertInfo "
                          "objects")
            self._db_migration_steps.remove(("1.0.0", "1.1.0"))
        msg = ("remaining db migration steps after call to "
               f"`_migrate_db_if_required`: {self._db_migration_steps}")
        self.log.debug(msg)
        if persist:
            self._persist(backup=backup)
        return db_data

    def _init_from_db(self) -> str:
        """Read JSON database."""
        db_fpath = Path(self.config.get("certalert_db_path"))
        self.log.info(f"loading certalert db from '{db_fpath}'")
        try:
            with open(db_fpath, "r") as fh:
                data = json.load(fh)
        except FileNotFoundError:
            self.log.warning(f"cannot find database '{db_fpath}'")
            return
        db_version = data.get("db_version")
        if db_version and (data.get("db_version") != self.__DB_VERSION__):
            msg = (f"program's db version ({self.__DB_VERSION__}) differs "
                   f"from database version ({db_version})!")
            self.log.warning(msg)
            try:
                self._db_migration_steps = self._get_db_migration_steps(
                    curr_v=db_version, target_v=self.__DB_VERSION__)
            except ValueError as e:
                msg = (f"cannot migrate certalert database from version "
                       f"{db_version} to version {self.__DB_VERSION__}: {e}")
                self.log.error(msg)
        data = self._migrate_db_if_required(db_data=data)
        ai_schema = CertAlertInfoSchema()
        for ai_json in data.get("alert_infos"):
            ai = ai_schema.load(ai_json)
            self.alert_infos.append(ai)
        return

    def _persist(self, backup=False) -> None:
        """Persist the certificate info to disk.

        If `backup` is set to True, create a backup of the existing db
        first.
        """
        if self.read_only:
            self.log.warning("operating in read-only mode, not persisting "
                             "data")
            return
        db_fpath = Path(self.config.get("certalert_db_path"))
        self.log.info(f"persisting data to {db_fpath}")
        ignore_fields = ["notification_count", "last_notification"]
        ai_schema = CertAlertInfoSchema(exclude=ignore_fields)
        ais = []
        data = {
            "db_version": self.__DB_VERSION__,
            "alert_infos": ais,
        }
        for ai in self.alert_infos:
            ais.append(ai_schema.dump(ai))
        with tempfile.NamedTemporaryFile(delete=False) as db_fh_tmp:
            db_fpath_tmp = Path(db_fh_tmp.name)
            with open(db_fpath_tmp, "w") as fh:
                fh.write(json.dumps(data))
                if backup:
                    ts = datetime.datetime.now(datetime.timezone.utc)
                    ts_s = datetime.datetime.strftime(ts, "%Y-%m-%d-%H%M%S")
                    bkp_fpath_s = (f"{db_fpath.parent}/{db_fpath.stem}_{ts_s}"
                                   f"{db_fpath.suffix}")
                    bkp_fpath = Path(bkp_fpath_s)
                    msg = (f"creating a backup of {db_fpath} as {bkp_fpath}")
                    self.log.info(msg)
                    shutil.copy(db_fpath, bkp_fpath)
            db_fpath_tmp.rename(db_fpath)
        self.log.info("data persisted")


DEFAULT_EMAIL_TEMPLATE_SUBJECT = ("[AUTOMATED] Certificate [notify_type] "
                                  "notification for [cert.name]")
DEFAULT_EMAIL_TEMPLATE_BODY = """**DEFAULT**

Certificate

'[cert.human_name]' ([cert.cert_id])

is about to expire on

[cert.not_valid_after].

You have [cert.days_left] days left to renew

The certificate expiry date was checked against data from
[cert.origin] > [cert.source]

Yours sincerely
certalert
"""
