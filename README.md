# `certalert`

`certalert` is a Python tool to monitor SSL certificate expiration dates and
notify responsible personnel before a certificate expires.

The monitoring is done via any (combination of) three options:

1. reading PEM files from a directory
1. reading (OpenSSL) `index.txt` files
1. fetching certificates via the network

Fetching the certificates via the network is, in a way, the most straight
forward approach. It has the benefit of "always being current" in the sense
that the monitoring always "sees" the current certificate.
If, for example after a notification, a certificate is replaced, the monitoring
will automgically start to monitor the new certficate.
The drawback is that the monitoring (usually) must have (direct) network access
(think: no proxy), however. And that the monitoring process itself is more
error prone than when reading files from disk (think: flaws in network
connectivity).

Reading PEM files is valuable for certificates that cannot be fetched via the
network, like client certificates, for example. Or when (direct) network access
is not an option.

Reading OpenSSL `index.txt` files/databases is a convenient option for private
CAs: the file basically comes "for free". The main drawback of reading
`index.txt` files is the missing (meta) information: especially there is no
fingerprints in the file.

The semantics of the monitoring differ slightly between the three sources:
fetching from the network is explained above. Reading from PEM files is
"static", meaning all certificates found in PEM files on disk are read and
will be considered relevant/active: notifications will be sent.
If a certificate is replaced/revoked or not "active" any more for other
reasons, it must be removed from `certalert`s PEM files and its replacement
certificate must be added (if any).
Reading from `index.txt` files is also somewhat "static", but `certalert` will
only consider entries that start with a "V" in `index.txt`. If an active CAs
`index.txt` is made available to `certalert`, it will not monitor revoked or
expired certificates.

`certalert` is desigend to check for expired certificates daily. It is also
possible to run it less frequently, but it is not advisable or beneficial to
run it more often.

Notifications are currently sent via email. The idea to (also) allow sending
messages via a messaging service (like Threema) is not implemented (yet?).

`certalert` remembers what notifications it has sent and will usually not
(immediately) send another notification for the same certificate.

## Installation

Python 3.10+ is required to run `certalert`. It is advisable to run it in its
own virtual environment. The following installation instructions show one way
how this can be done.

It is not necessary to "install" `certalert`. It can just be downloaded and
extracted into a folder and will run from there. `certalert` is NOT available
from PyPi/via `pip` because it is an application, not a library.

### Required Python modules

`certalert` has only three requirements:

* `ruamel.yaml` (v0.17.21)
* `python-dateutil` (v2.8.2)
* `marshmallow` (v3.19.0)

The requirements are listed in `requirements.txt`. The installation process
below shows an easy way to install them.

### Download and extract

Download the desired version of `certalert` from Gitlab:

[`certalert` on Gitlab](https://gitlab.com/scherand/certalert)

Go to "Tags", select the desired version (usually the most recent one) and
download it via the respective button in the top right corner. It does not
matter what archive fromat you pick as long as you can extract it later.

Now, choose/create a folder on the machine you want to run `certalert` on.
For \*nix systems, `/usr/local/opt/certalert` could be an option. Then,
extract the archive you just downloaded into this folder.

You might want to use `tar`'s `--strip-components 1` feature (if you use (GNU)
tar) or extract from the parent directory to avoid ending up with a duplicate
`certalert` in the path (i.e. `/usr/local/opt/certalert/certalert/` in the
example above).

### The virtual environment

This is how a virtual environment for `certalert` can be set up using only
Python (on the shell).

Choose a directory where your virtual environment will "live" in. If you do
not have any other preferences, the directory you earlier extracted `certalert`
into works just fine. Change to the directory you have chosen:

    $ cd /path/to/certalert/directory

Next, create the virtual environment. When you use the following command, it
will be called `.certalert` and installed into a subirectory of the current
directory. The subdirectory will also be called `.certalert`:

    $ python -m venv .certalert

On some systems, `python` does not work. Try to use `python3` then. Or,
especially on Windows, maybe just `py`. If any of this works, but plain
`python` does not, you will have to substitute `python` in all the following
commands.

To work with the virtual environment, you have to activate it. You do this
using one of the "activation" scripts in `bin`. Which one depends on the shell
you use. This is the command to activate the new virtual environment in a C
shell:

    $ source /path/to/certalert/directory/.certalert/bin/activate.csh

For a bash shell it would be:

    $ . /path/to/certalert/directory/.certalert/bin/activate

For other options, check
[the official venv documentation](https://docs.python.org/3.10/library/venv.html#how-venvs-work).

Most likely your prompt will change to somehow include `.certalert` to indicate
that the virtual environment is active.

Note: to deactivate the virtual environment, just type

    $ deactivate

#### Installing the required Python packages

*With the virtual environment active*, use the following command to install
`certalert`s dependencies:

    $ python -m pip install -r requirements.txt

#### Testing the installation

If all went well, you should now be able to successfully summon up
`certalert`s help text on your screen. You have to run this command from the
directory you have extracted `certalert` into:

    $ python -m certalert.certalert -h

This should display `certalert`s help text. If it does not, you need to
troubleshoot before continuing...

The help text looks something like this:

    usage: certalert.py [-h] [-v] [--verbose] [-c CONFIG] [--include-certs-from-config] {notify,show,migrate,ignore,purge,test-email} ...

    options:
      -h, --help            show this help message and exit
    ...

## Configuration

`certalert`s main configuration file is a YAML file. By default `certalert` is
looking for `certalert.yml` in the directory you run it from (i.e. the
directory you have extracted `certalert` into). The `-c/--config` command line
switch allows to use a different file.

An example config file `certalert_dist.yml` showing all options is provided
with `certalert`. All options have (sane) default values. The options are now
explained here.

### `certalert_db_path`

    certalert_db_path: certalert.json

The value is the path (including the file name) to the `certalert` "database"
(a JSON file). `certalert` will use this file to persist information about
notifications that have been sent already, etc.
The default value is `certalert.json`.

If, as in the example, just a filename is provided, the file will be located
in the directory you have extracted `certalert`.

You might want to back up this file to keep `certalert`s state in case of a
disaster!

### `notification_recipients`

    notification_recipients:
      - name: admin
        email: chef@mailinator.com
        default: true
      - name: worker
        email: workhorse@mailinator.com
        threema_id: AB7C6DEF
        default: false

All recipients who should be able to recieve notifications must be listed
here. They will later be referenced by their `name` in the config. The `name`
can be any string, but it should be unique within a config.

The `email` field holds the email address notifications will be sent to for
this recipient.

When `default` is set to `true`, this recipient will receive *all notifications
that do not have any specific recipients defined* (see `recipients` key of
later options).

The default value is an empty list.

*Note that the `threema_id` is not (yet?) used!*

### `notification_schemes`

    notification_schemes:
      - name: default
        heads_up_days: 90
        notification_threshold: 30
        notification_frequency: 5
        warning_threshold: 14
        warning_frequency: 2
        alert_threshold: 7
        alert_frequency: 1
        post_mortem_duration: 30
      - name: letsencrypt
        heads_up_days: 30
        notification_threshold: 14
        notification_frequency: 2
        warning_threshold: 6
        warning_frequency: 2
        alert_threshold: 3
        alert_frequency: 1
        post_mortem_duration: 10

Notification schemes define how `certalert` decides if a notification should
be sent or not. The various schemes will later be referenced by their `name`
in the config.

**heads\_up\_days** defines how many days before expiration the first
notification will be sent. This notification's type is called "HEADS_UP".
In the example above, the HEADS_UP notification will be sent 90 days before
expiration for certificates that have their notification scheme set to
`default` and 30 days before expiration for certificates that have their
notification scheme set to `letsencrypt`.

**notification_threshold** defines how many days before expiration `certalert`
starts to sent "NOTIFY" notifications. 30 days (14 for "letsencrypt",
respectively) before certificate expiration in the example above.

**notification_frequency** defines how many days `certalert` is waiting before
sending another notification of the same type. This means if a NOTIFY
notification was sent on day X and the certificate does not change within
`notification_frequency` days, another NOTIFY notification will be sent on day
X + notification_frequency.

**warning_threshold** is the same as `notification_threshold` (see above), but
for notifications of type "WARNING".

**warning_frequency** is the same as `notification_frequency` (see above), but
for notifications of type "WARNING".

**alert_threshold** is the same as `notification_threshold` (see above), but
for notifications of type "ALERT".

**alert_frequency** is the same as `notification_frequency` (see above), but
for notifications of type "ALERT".

**post\_mortem\_duration** defines how many days *after* expiration of a
certificate "POST_MORTEM" notifications will be sent out. POST_MORTEM
notifications are sent for every run of `certalert` until
`post_mortem_duration` days have passed.

The default value is an empty list.

How certificates "know" what notification scheme they should use is explained
later (in the `notification_scheme` key).

### `dirs_notification_templates_email`

    dirs_notification_templates_email:
      - dir: email_templates
        default: true

Indicates directories that hold notification templates for email notifications.
See "[Templates (email)](#templates-email)" below for how an email
notification template must look like.

The `dir` key is used later to reference these templates (see
`templates_email` key).

When `default` is set to `true`, the templates in this directory are used if
no more specific template directory is specified for a certificate.
Note that there can only be one template directory that has `default` set
`true`!

The directory referenced by `dir` should contain files that can have any of
the following names:

* `default.txt`
* `heads_up.txt`
* `notify.txt`
* `warning.txt`
* `alert.txt`
* `post_mortem.txt`

As you can probably guess, the files are used as the template for the
notification type indicated by their name. If no specific template is available
for a type, the `default.txt` template is used.

The default value is an empty list. A default email template is hardcoded
in `certalert` and will be used if no custom templates are defined.

#### Templates (email)

A template file should have a line that starts with "Subject:". The text that
should be used as the subject of the email message should follow without any
whitespace after the colon.

Then, the message body should follow. This text can be "plain text formatted"
(i.e. contain whitespace/newlines) and any of the following placeholders.

Template files for email messages can contain the following placeholders (for
both, subject and body):

**[notify_type]**  
A string indicating the type of the notification, this is any of HEADS_UP,
NOTIFY, WARNING, ALERT or POST_MORTEM. The type is determined by the number
of days left until certificate expiration (or the number of days past since
expiration) and the `notification_scheme` in use for the particular
certificate.

**[cert.name]**
The value of the `.name` property of the certificate. In most cases, this
equals the common name (CN) of the certificate.

**[cert.human_name]**
A "human readable" identifier for the certificate. It consists of the `.name`
property of the certificate, followed by its `.serial` (in hex and decimal)
and the `.cert_id` (usually the certificate's SHA256 fingerprint) shortened to
14 characters.

An example human readable name is:

    *.badssl.com [0x4ae79549fa9abe3f100f17a478e16909 (99565320202650452861752791156765321481), BA:10:5C:E0:2B]

**[cert.cert_id]**
The certificates `.cert_id` property. The `cert_id` is a certalert internal
unique identifier for a certificate. If a certificate has a `.fingerprint`,
which is usually the case, the `.cert_id` is identical to the fingerprint.
The fingerprint is the certfificate's SHA256 fingerprint.

**[cert.not_valid_after]**
The date the certificate will expire. The format is ("YYYY-mm-dd HH:MM:SS+TZ",
e.g. 2023-04-29 16:17:29+00:00).

**[cert.days_left]**
The number of days until the certificate expires. For certificates that have
already expired, this number is negative, indicating the number of days since
the certificate expired.

**[cert.origin]**
The certificate's `.origin` property, indicating where the certificate data
originated from.

For certificates loaded from PEM or Index, this is the path
to the folder the certificates file is located in.

For certificates loaded from URL it is the URL.

**[cert.source]**
The certificate's `.source` property, more precisely indentifying the
certificate's source within the "origin".

For certificates loaded from PEM or Index, this is the name of the file the
certificates is located in.

For certificates loaded from URL it is the domain name.

### `dirs_pem`

    dirs_pem:
      - dir: my_cas_pem
        recipients:
          - admin
          - worker
        notification_scheme: letsencrypt
        templates_email: email_templates
      - dir: my_second_cas_pem
  
`dir` indicates the directory `certalert` will use to look for PEM files. Any
file in the directory ending in `*.pem` will be considered.

`recipients` is a list of recipients (their names as defined in
`notification_recipients`) who will receive notifications for *all*
certificates/PEM files in this directory.
If the key is omitted, all default recipients will be notified.
If any recipient is defined here, the default recipients will *not* be
notified! However, they can, of course, also be listed in `recipients` by name.

`notifictation_scheme` is the name of a notifcation scheme defined in
`notification_schemes`. This will define when and how often `certalert` will
send notifications for certs/PEM files in this directory.
If the key is omitted, the `default` notification scheme will be used.

The default value is an empty list.

### `dirs_index`

    dirs_index:
     - dir: my_cas_idx

Very similar to `dirs_pem` but for `index.txt` files.

`dir` indicates the directory `certalert` will use to look for Index files.
Any file in the directory ending in `*.txt` will be considered.

Note that only entries starting with "`V`" are considered!

`recipients` is a list of recipients (their names as defined in
`notification_recipients`) who will receive notifications for *all*
certificates/Index files in this directory.
If the key is omitted, all default recipients will be notified.
If any recipient is defined here, the default recipients will *not* be
notified! However, they can, of course, also be listed in `recipients` by name.

`notifictation_scheme` is the name of a notifcation scheme defined in
`notification_schemes`. This will define when and how often `certalert` will
send notifications for certs/Index files in this directory.
If the key is omitted, the `default` notification scheme will be used.

The default value is an empty list.

### `urls`

    urls:
      - domain: badssl.com
        recipients:
          - admin
        notification_scheme: default
      - domain: expired.badssl.com
        notification_scheme: letsencrypt
      - domain: imap.example.com
        port: 993
      - domain: https://test.example.com
        timeout: 1

A list of "URLs" (domains) that should be monitored. `certalert` will connect
to the domains of these URLs when it runs and check the validity of the
certificate it just received. This is sort of a "live" monitoring.

`domain` is the domain (or URL) to connect to.

`port` defines the TCP port to use for the connection; by default the HTTPS
port 443 is used.

`timeout` (in seconds) defines how long `certalert` tries to connect to the
domain before giving up. If this key is omitted, the default timeout value
is used (see below: `url_conn_timeout`).

`recipients` is a list of recipients (their names as defined in
`notification_recipients`) who will receive notifications when this URLs
certificate is about to expire.
If the key is omitted, all default recipients will be notified.
If any recipient is defined here, the default recipients will *not* be
notified! However, they can, of course, also be listed in `recipients` by name.

`notifictation_scheme` is the name of a notifcation scheme defined in
`notification_schemes`. This will define when and how often `certalert` will
send notifications for this URL.
If the key is omitted, the `default` notification scheme will be used.

The default value is just `expired.badssl.com`. This is used if the `urls` key
is not present in the config. Use

    urls: []

in the config to disable the default.

### `url_conn_timeout`

    url_conn_timeout: 10

Defines the default timeout for URL connections (see above).
The default value is 10 seconds.

### `smtp_server`

    smtp_server:
      host: smtp.gmail.com
      port: 587
      username: null
      password: null
      sender: Cert Alert <certalert@mailinator.com>

Configures the SMTP server (aka smarthost in an MS Exchange environment) used
for sending email notifications. In most cases it is recommended to use the
default SMTP submission port 587. Another common choice is 25. In rare cases
you might also try (the de-facto deprecated) port 465.

Setting `username` enables SMTP authentication.

`certalert` will try to use `STARTTLS` when connecting to the SMTP server. It
does so in a very optimistic way though: no certificate checks are done and
if `STARTTLS` fails a non-encrypted connection will silently be established.

### `certificates`

    certificates:
      - fingerprint: AF:8D:E0:C3:D8:D3:E5:58:A7:C0:22:3C:2E:73:ED:9C:15:B8:66:31
        serial: 00:B0
        issuer: my.ca.local
        name: CN:www.example.com
        origin: config.yml
        source: hardcoded cert 1
        valid_from_str: "2023-02-27 09:10:11+03:00"
        valid_until_str: "2023-03-27T08:09:10Z"
        notification_scheme_name: default  # 'default' would not strictly be required
        recipients:
          - admin
          - worker

A list of (possibly dummy) certificates that are considered only if the command
line switch `--include-certs-from-config` is used.

The values for `fingerprint`, `serial`, `issuer`, `name`, `origin` and `source`
are arbitrary. `name` "usually" corresponds to the common name (CN) of a
certificate.
`origin` and `source` are meant to enable you and/or your users to locate the
"source" of a notification.

`valid_from_str` and `valid_until_str` define the `not_before` and `not_after`
date (and time) for the cert. The strings must be parsable to a `datetime` by
`dateutil.parser.parse`.
As an alternative, you can use `valid_from_dt` and `valid_until_dt` instead
of `valid_from_str` and `valid_until_str` and provide `datetime` objects
yourself.

`recipients` is a list of recipients (their names as defined in
`notification_recipients`) who will receive notifications when this certificate
is about to expire.
If the key is omitted, all default recipients will be notified.
If any recipient is defined here, the default recipients will *not* be
notified! However, they can, of course, also be listed in `recipients` by name.

`notifictation_scheme` is the name of a notifcation scheme defined in
`notification_schemes`. This will define when and how often `certalert` will
send notifications for this certificate.
If the key is omitted, the `default` notification scheme will be used.

The default value is an empty list.

## Running

Given the following (example) directory structure, `certalert` should be run
using the commands explained in this section.

    CertAlert
    |-- certalert
        |-- __init__.py
        |-- cert_loader.py
        |-- certalert.py
        |-- ...
    |-- certalert_dist.yml
    |-- certalert.json
    |-- email_templates
        |-- default.txt
    |-- tests
        |-- data
            |-- idx
            |-- pem
        |-- __init__.py
        |-- conftest.py
        |-- context.py
        |-- test_cert.py
        |-- ...
    |-- ...

### Using the script's functionality

The basic invocation of `certalert` is as follows:

    $ pwd
    .../CertAlert/
    $ python -m certalert.certalert -h

Technically this command runs `__main__()` from `certalert.py`.

All commands take an optional `--config` argument to specify a non-default
path to the config file to use. The default location `certalert` is looking
for its config (`certalert.yml`) is in the `CertAlert` folder (see above).

For all commands: adding the `--verbose` flag makes the output/logging more
verbose.

To initiate a check of all known certificates and send any desired
notifications, call `certalert` as follows. **This is the command that you
want to issue on a regular basis to use `certalert`s functionality.**

    $ python -m certalert.certalert notify

It is possible to "ingore" a certificate by providing (a prefix of) its
`cert_id` to the `ignore` command's `-add` argument:

    $ python -m certalert.certalert ignore -add AA:BB:CC

For ignored certificates, no notifications are sent but the rest remains the
same. This means for example that the certificate is still loaded/fetched
(meaning `certalert` still opens a network connection to the server if the
certificate is defined as an URL or reads the file if it is defined as a PEM).
This behaviour is mainly defined by the inner workings of `certalert`: state
data about the "history" of a certficate and its processing (and its "ingore
state") is mapped via the `.cert_id`. But the `.cert_id` is only known *after*
the certificate was read/loaded. So its "ignored" state can only be checked
after loading it.

Ignoring a certificate is at least useful in the following scenarios:

* a certificate from an `index.txt` file is also defined as an URL and the URL
  should be kept because it provides a "live view"
* a certificate is known to be unused/not relevant but its source cannot be
  removed from `certalert` (because an automated process manages the sources,
  for example)

A certificates "ignore" status can be reset using the `--remove` argument:

    $ python -m certalert.certalert ignore --remove AA:BB:CC

To see a list of all currently ignored certificate Ids, use the `--list` flag:

    $ python -m certalert.certalert ignore --list

When using the `ignore` command with the `--list` flag, the output can also be
JSON. Just add an additional `--json` flag to receive JSON on `STDOUT` (the
log messages are written to `STDERR`):

    $ python -m certalert.certalert ignore --list --json

To see (some) information about the certificates `certalert` "knows about",
you can use the `show` command. Like so:

    $ python -m certalert.certalert show

"Ignored" certificates are marked with an asterisk ("\*") as the first
character of the line in this output.

There is an option to retrieve the output of the `show` command as JSON. Add a
`--json` flag to indicate this and receive JSON on `STDOUT` (the log messages
are written to `STDERR`):

    $ python -m certalert.certalert show --json

If you want to suppress the log messages, you can use the standard \*NIX method
of redirecting `STDERR` to `/dev/null`, for example.

    $ python -m certalert.certalert show --json 2>/dev/null

It is possible to instruct `certalert` to "forget" everything it knows about a
certificate (Id) using the `purge` command:

    $ python -m certalert.certalert purge AA:BB:CC

The value passed to `purge` is treated as a prefix for a `cert_id`: if
multiple certificate Ids matching this prefix are found, the command will ask
for a confirmation before purging all matching certificate's information.
To silence this asking for permission, the `--force` flag can be passed to the
`purge` command:

    $ python -m certalert.certalert purge --force AA:BB:CC

If only a single `cert_id` matches the value passed to `purge`, the `--force`
flag has no effect.

The `test-email` command can be used to test if sending email notifications
(with the indicated configuration) is possible. Use the command as follows:

    $ python -m certalert.certalert test-email recipient@example.com

This will (try to) send a "NOTIFY" email notfication to recipient@example.com.

To send a different type of notification (for example to test the corresponding
email template) invoke the command with an additional `--notification-type`
argument:

    $ python -m certalert.certalert test-email recipient@example.com  --notification-type HEADS_UP

The known email types are:

* HEADS_UP
* NOTIFY
* WARNING
* ALERT
* POST_MORTEM

A command to migrate an existing database (JSON file) that is using an old(er)
data format/scheme than the current verison of `certalert` is available for
special purposes (most likely: development of `certalert`). During normal
operation, the command should not be required. The command is aptly named
`migrate` :)

When the database is migrated using this command, a backup of the current
database will be created (in the database's folder).

    $ python -m certalert.certalert migrate

Note that all certificates will be loaded from source when this command is
run. This is mainly because database transformations might require additional
information/data which is only available from the source.

Also note that the database transformation is also (implicitly) done when
another subcommand (e.g. `notify`) is called on an "old" database. In contrast
to when using the `migrate` command, *no* backup of the existing database is
created then.
The reasoning behind this is that `certalert` is not designed to work with
"old" dabases and *must* migrate them. A backup solution for the "old"/existing
database is hopefully in place anyway...

### Tests

This section is only interesting if you want to develop for `certalert` or if
you are a curious person. It is not required for "just" running/using
`certalert`.

To be able to use these commands, the virtual environment you use must have
the modules listed in `requirements.txt` AND those in `requirements-dev.txt`
installed.

The following command uses the [`pytest`](https://docs.pytest.org/en/7.2.x/)
test runner to run all tests it can find:

    $ pwd
    .../CertAlert/
    $ python -m pytest

Or, with coverage report:

    $ pwd
    .../CertAlert/
    $ python -m pytest --cov=certalert

The coverage output contains a table that lists the test coverage (percentage
of LoC) that were "hit" (executed) during the tests.

Note that `certalert.py` is excluded from the coverage report because it is
the interactive part of `certalert` and cannot (easily) be tested
automatically.

Test coverage of `cert_loader.py` and `certalert_store.py` is lower because the
integration tests required to test those modules are not (yet) implemented.

Also note that a 100% coverage does *not* imply that everything is working
correctly under all circumstances.

    Name                               Stmts   Miss  Cover
    ------------------------------------------------------
    certalert/__init__.py                 15      0   100%
    certalert/cert_loader.py             173     13    92%
    certalert/certalert_cert.py           73      2    97%
    certalert/certalert_info.py           57      0   100%
    certalert/certalert_store.py         446    115    74%
    certalert/notification.py             56      0   100%
    certalert/notification_scheme.py      38      0   100%
    certalert/recipient.py                30      0   100%
    ------------------------------------------------------
    TOTAL                                888    130    85%

and then, possibly, check using

    $ pwd
    .../CertAlert/
    $ coverage report -m

### Log

`certalert` is storing its log in a file called `certalert.log` located in the
same directory you extracted the code into.

## Ideas

* Do all the logging via syslog (no log file)
* Summary notification once per month (to whom?)
* Notification via Threema (or other messengers)
* Add option to get JSON output from all commands (e.g. `notfiy`)
* Users from LDAP or similar
* Use a proxy when fetching certs (possible/usable with SSL interception
  and/or caching?)
* Trigger "generic action" instead of "just" a notification (think: ACME, API)
* Integration tests (for everything that involves loading the DB first, etc.)
* Is the decision that certificates marked "expired" in `index.txt` are not
  monitored correct?
