# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

## [2.3.1] - 2024-03-05

### Fixed

- Convert `src` into a string for `replace` when cert is from a directory

## [2.3.0] - 2023-03-31

### Changed

- Display "valid until" date in output of `show` command

### Added

- Include version number in startup message

## [2.2.0] - 2023-03-22

### Added

- Command `migrate` to transfor the database from an earlier version of
  `certalert` into a newer one (without a `notify` run); cf commit db3de452

### Changed

- JSON output (`--json`) contains `not_before` and `not_after` dates

## [2.1.0] - 2023-03-15

### Added

- Option `--json` to get return values and output as JSON (!3)

### Changed

- The value of the `origin` attribute of certificates loaded from "URL"s is
  now "network" (!2)

### Fixed

- Bug that prevented `certalert` from sending any notifications

## [2.0.0] - 2023-03-13

### Added

- This changelog
- Command to display `certalert`'s version
- Command to list all currently ignored certificate Ids

### Changed

- Converted arguments (`--notify`, `--show`, etc.) to subcommands (!1)
- Updated README.md to reflect new command line usage

## [1.0.0] - 2023-03-11

### Added

- Initial version of `certalert`, see README.md

[unreleased]: https://gitlab.com/scherand/certalert/-/tree/main
[2.3.0]: https://gitlab.com/scherand/certalert/-/tags/2.3.0
[2.2.0]: https://gitlab.com/scherand/certalert/-/tags/2.2.0
[2.1.0]: https://gitlab.com/scherand/certalert/-/tags/2.1.0
[2.0.0]: https://gitlab.com/scherand/certalert/-/tags/2.0.0
[1.0.0]: https://gitlab.com/scherand/certalert/-/tags/1.0.0
